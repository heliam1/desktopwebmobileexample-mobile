import 'package:desktopwebmobileexample_mobile/auth/resetActions.dart';
import 'package:desktopwebmobileexample_mobile/home/homeActions.dart';
import 'package:desktopwebmobileexample_mobile/home/homeReducer.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  group('HomeReducer', () {

    test('Initial Home State', () {
      HomeState expectedInitialState = HomeState(
        loading: false,
        error: '',
        success: false,
      );
      HomeState actualInitialState = initialHomeState;
      expect(actualInitialState, expectedInitialState);
    });

    test('AnonLogIn does not change state', () {
      HomeState currentState = new HomeState(
        loading: false,
        error: '',
        success: false,
      );
      HomeAction action = anonLogIn();
      HomeState expectedNextState = new HomeState(
        loading: false,
        error: '',
        success: false,
      );

      HomeState actualNextState = homeReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('AnonLogInSuccess does not change state', () {
      HomeState currentState = new HomeState(
        loading: false,
        error: '',
        success: false,
      );
      HomeAction action = anonLogInSuccess();
      HomeState expectedNextState = new HomeState(
        loading: false,
        error: '',
        success: false,
      );

      HomeState actualNextState = homeReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('SignUp', () {
      HomeState currentState = new HomeState(
        loading: false,
        error: '',
        success: false,
      );
      HomeAction action = signUp('email', 'pw');
      HomeState expectedNextState = new HomeState(
        loading: true,
        error: '',
        success: false,
      );

      HomeState actualNextState = homeReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('SignUpSuccess shows success', () {
      HomeState currentState = new HomeState(
        loading: true,
        error: '',
        success: false,
      );
      HomeAction action = signUpSuccess();
      HomeState expectedNextState = new HomeState(
        loading: false,
        error: '',
        success: true,
      );

      HomeState actualNextState = homeReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('LogIn after SignUpSuccess', () {
      HomeState currentState = new HomeState(
        loading: false,
        error: '',
        success: true,
      );
      HomeAction action = logIn('email', 'pw');
      HomeState expectedNextState = new HomeState(
        loading: true,
        error: '',
        success: false,
      );

      HomeState actualNextState = homeReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('LogInSuccess', () {
      HomeState currentState = new HomeState(
        loading: true,
        error: '',
        success: false,
      );
      HomeAction action = logInSuccess();
      HomeState expectedNextState = new HomeState(
        loading: false,
        error: '',
        success: false,
      );

      HomeState actualNextState = homeReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('LogIn after HomeError and SignUpSuccess', () {
      HomeState currentState = new HomeState(
        loading: false,
        error: 'Failed.',
        success: true,
      );
      HomeAction action = logIn('email', 'pw');
      HomeState expectedNextState = new HomeState(
        loading: true,
        error: '',
        success: false,
      );

      HomeState actualNextState = homeReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('HomeError', () {
      HomeState currentState = new HomeState(
        loading: true,
        error: '',
        success: false,
      );
      HomeAction action = homeError('Failed.');
      HomeState expectedNextState = new HomeState(
        loading: false,
        error: 'Failed.',
        success: false,
      );

      HomeState actualNextState = homeReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Error after SignUpSuccess', () {
      HomeState currentState = new HomeState(
        loading: true,
        error: '',
        success: true,
      );
      HomeAction action = homeError('Failed.');
      HomeState expectedNextState = new HomeState(
        loading: false,
        error: 'Failed.',
        success: false,
      );

      HomeState actualNextState = homeReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Reset', () {
      HomeState currentState = new HomeState(
        loading: false,
        error: 'Failed.',
        success: false,
      );
      Reset action = reset();
      HomeState expectedNextState = initialHomeState;

      HomeState actualNextState = homeReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

  });
  
}
