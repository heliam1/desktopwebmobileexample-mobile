import 'package:desktopwebmobileexample_mobile/AppState.dart';
import 'package:desktopwebmobileexample_mobile/auth/Auth.dart';
import 'package:desktopwebmobileexample_mobile/clientLogging/ClientLoggingFake.dart';
import 'package:desktopwebmobileexample_mobile/home/homeActions.dart';
import 'package:desktopwebmobileexample_mobile/home/homeEpic.dart';
import 'package:desktopwebmobileexample_mobile/home/homeReducer.dart';
import 'package:desktopwebmobileexample_mobile/nav/NavFake.dart';
import 'package:desktopwebmobileexample_mobile/rootReducer.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux/redux.dart';

class MockAuth extends Mock implements Auth {}

void main() {

  group('HomeEpic', () {

    //AppState state = AppState();

    setUpAll(() {});
    setUp(() {
      //state = AppState(
      //  homeState: initialHomeState,
      //);
    });
    tearDown(() {});
    tearDownAll(() {});

    test('SignUpError does not affect future SignUpSuccess.', () async {
      final auth = MockAuth();

      var answers = [
        (_) => Future.error(StateError('Failed.')),
        Future.value(),
      ];

      when(auth.signUpEmailAndPassword('example@example.com', 'Example123!'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_);
        }
        return answer;
      });

      Stream<dynamic> action$ = Stream.fromIterable([
        signUp('example@example.com', 'Example123!'),
        signUp('example@example.com', 'Example123!'),
      ]);
      EpicStore<dynamic> state$ = EpicStore<AppState>(
        Store<AppState>(
          rootReducer,
          initialState: AppState(
            homeState: initialHomeState,
          ),
          syncStream: false,
          distinct: true,
        ),
      );

      Stream epic$ = homeSignUpEpic(auth)(action$, state$);

      List<dynamic> result = await epic$.toList();

      expect(result, equals([
        homeError('Bad state: Failed.'),
        signUpSuccess(),
      ]));
    });

    test('LogInError does not affect future LogInSuccess.', () async {
      final nav = NavFake();
      final clientLogging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      final auth = MockAuth();

      var answers = [
        (_) => Future.error(StateError('Failed.')),
        Future.value(),
      ];

      when(auth.logInEmailAndPassword('example@example.com', 'Example123!'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_);
        }
        return answer;
      });

      Stream<dynamic> action$ = Stream.fromIterable([
        logIn('example@example.com', 'Example123!'),
        logIn('example@example.com', 'Example123!'),
      ]);
      EpicStore<dynamic> state$ = EpicStore<AppState>(
        Store<AppState>(
          rootReducer,
          initialState: AppState(
            homeState: initialHomeState,
          ),
          syncStream: false,
          distinct: true,
        ),
      );

      Stream epic$ = homeLogInEpic(
        auth,
        nav,
        clientLogging,
        userAgent,
      )(action$, state$);

      List<dynamic> result = await epic$.toList();

      expect(result, equals([
        homeError('Bad state: Failed.'),
        logInSuccess(),
      ]));
    });

    test('AnonLogInError on Auth.anonLogIn fail.', () async {
      final clientLogging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      final auth = MockAuth();

      var answers = [
        (_) => Future.error(StateError('Failed.')),
      ];

      when(auth.logInAnonymously())
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        return answer(_);
      });

      Stream<dynamic> action$ = Stream.fromIterable([
        anonLogIn(),
      ]);
      EpicStore<dynamic> state$ = EpicStore<AppState>(
        Store<AppState>(
          rootReducer,
          initialState: AppState(
            homeState: initialHomeState,
          ),
          syncStream: false,
          distinct: true,
        ),
      );

      Stream epic$ = homeAnonLogInEpic(
        auth,
        clientLogging,
        userAgent,
      )(action$, state$);

      List<dynamic> result = await epic$.toList();

      expect(result, equals([
        homeError('Bad state: Failed.'),
      ]));
    });

    test('AnonLogInSuccess on Auth.anonLogIn success.', () async {
      final clientLogging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      final auth = MockAuth();

      var answers = [
        Future.value(),
      ];

      when(auth.logInAnonymously())
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        return answer;
      });

      Stream<dynamic> action$ = Stream.fromIterable([
        anonLogIn(),
      ]);
      EpicStore<dynamic> state$ = EpicStore<AppState>(
        Store<AppState>(
          rootReducer,
          initialState: AppState(
            homeState: initialHomeState,
          ),
          syncStream: false,
          distinct: true,
        ),
      );

      Stream epic$ = homeAnonLogInEpic(
        auth,
        clientLogging,
        userAgent,
      )(action$, state$);

      List<dynamic> result = await epic$.toList();

      expect(result, equals([
        anonLogInSuccess(),
      ]));
    });

  });
}
