import 'package:desktopwebmobileexample_mobile/AppState.dart';
import 'package:desktopwebmobileexample_mobile/auth/Auth.dart';
import 'package:desktopwebmobileexample_mobile/auth/AuthFake.dart';
import 'package:desktopwebmobileexample_mobile/auth/authEpic.dart';
import 'package:desktopwebmobileexample_mobile/auth/authReducer.dart';
import 'package:desktopwebmobileexample_mobile/clientLogging/ClientLoggingFake.dart';
import 'package:desktopwebmobileexample_mobile/home/HomePage.dart';
import 'package:desktopwebmobileexample_mobile/home/homeEpic.dart';
import 'package:desktopwebmobileexample_mobile/home/homeReducer.dart';
import 'package:desktopwebmobileexample_mobile/main.dart';
import 'package:desktopwebmobileexample_mobile/nav/Nav.dart';
import 'package:desktopwebmobileexample_mobile/nav/NavImpl.dart';
import 'package:desktopwebmobileexample_mobile/ui/FadeOut.dart';
import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:redux_epics/redux_epics.dart';

Widget setup(
  Widget ui,
  NavImpl nav, //incl. nav state
  AppState preloadedState,
  Dependencies dependencies,
) {
  final store = Store<AppState>(
    /* reducer: */ (AppState state, action) {
      return new AppState(
        authState: authReducer(state.authState, action),
        homeState: homeReducer(state.homeState, action),
      );
    },
    middleware: [
      new EpicMiddleware(
        combineEpics<dynamic>([
          signedInEpic(dependencies.nav),
          homeAnonLogInEpic(dependencies.auth, dependencies.logging, dependencies.userAgent),
          homeLogInEpic(dependencies.auth, dependencies.nav, dependencies.logging, dependencies.userAgent),
          homeSignUpEpic(dependencies.auth),
        ]),
      ),
    ],
    initialState: preloadedState,
    syncStream: false,
    distinct: true,
  );

  return StoreProvider(
    store: store,
    child: MaterialApp(
      navigatorKey: nav.nav,
      title: 'Desktop Web Mobile',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          builder: (context) => ui
        );
      },
    ),
  );
}

class MockAuth extends Mock implements Auth {}

void main() {

  // IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('HomePage', () {    
    testWidgets('Initial Render', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());

      Widget page = setup(
        HomePage(),
        nav,
        AppState(
          authState: initialAuthState,
          homeState: initialHomeState,
        ),
        Dependencies(
          nav: nav,
          auth: AuthFake(),
          logging: ClientLoggingFake(),
          userAgent: 'mobile-test',
        ),
      );
      
      // Build app and trigger a frame.
      // Wait for all async operations to finish.
      await tester.pumpWidget(page);
      await tester.pumpAndSettle();

      expect(find.text('CHRONOS'), findsOneWidget);
      expect(find.byType(ProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
      // TODO expect no success icon
      expect(find.text('Login'), findsOneWidget);
      expect(find.text('Sign Up'), findsOneWidget);
      // TODO expect some content 1
      // TODO expect some content 2

    });

    testWidgets('Sign up fails, then succeeds', (WidgetTester tester) async {
      final nav = NavImpl(GlobalKey<NavigatorState>());
      // https://pub.dev/packages/mockito
      // By default, all methods return null.
      final auth = MockAuth();

      var answers = [
        (_) => Future.error(StateError('Failed.')),
        // (_) => Future.delayed(Duration(milliseconds: 200), () => null),
        Future.value(),
      ];

      when(auth.logInAnonymously())
      .thenAnswer((_) => Future.value());

      when(auth.signUpEmailAndPassword('example@example.com', 'Example123!'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_);
        }
        return answer;
      });
      
      Widget page = setup(
        HomePage(),
        nav,
        AppState(
          authState: initialAuthState,
          homeState: initialHomeState,
        ),
        Dependencies(
          nav: nav,
          // auth: AuthFake(),
          auth: auth,
          logging: ClientLoggingFake(),
          userAgent: 'mobile-test',
        )
      );

      await tester.pumpWidget(page);
      await tester.pumpAndSettle();

      await tester.enterText(find.widgetWithText(TextField, 'Email'), 'example@example.com');
      await tester.enterText(find.widgetWithText(TextField, 'Password'), 'Example123!');
      await tester.tap(find.text('Sign Up'));

      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsOneWidget);

      // await tester.enterText(find.text('Email'), 'example@example.com');
      // await tester.enterText(find.text('Password'), 'Example123!');
      await tester.tap(find.text('Sign Up'));

      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(ProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
    });

    testWidgets('Log In fails, then succeeds', (WidgetTester tester) async {
      final nav = NavImpl(GlobalKey<NavigatorState>());
      final auth = MockAuth();

      var answers = [
        (_) => Future.error(StateError('Failed.')),
        Future.value(),
      ];

      when(auth.logInAnonymously())
      .thenAnswer((_) => Future.value());

      when(auth.logInEmailAndPassword('example@example.com', 'Example123!'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_);
        }
        return answer;
      });
      
      Widget page = setup(
        HomePage(),
        nav,
        AppState(
          authState: initialAuthState,
          homeState: initialHomeState,
        ),
        Dependencies(
          nav: nav,
          // auth: AuthFake(),
          auth: auth,
          logging: ClientLoggingFake(),
          userAgent: 'mobile-test',
        )
      );

      await tester.pumpWidget(page);
      await tester.pumpAndSettle();

      await tester.enterText(find.widgetWithText(TextField, 'Email'), 'example@example.com');
      await tester.enterText(find.widgetWithText(TextField, 'Password'), 'Example123!');
      await tester.tap(find.text('Login'));

      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(ProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsOneWidget);

      // await tester.enterText(find.text('Email'), 'example@example.com');
      // await tester.enterText(find.text('Password'), 'Example123!');
      await tester.tap(find.text('Login'));

      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(ProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
    });

  });
}
