import 'package:desktopwebmobileexample_mobile/AppState.dart';
import 'package:desktopwebmobileexample_mobile/auth/AuthFake.dart';
import 'package:desktopwebmobileexample_mobile/auth/authActions.dart';
import 'package:desktopwebmobileexample_mobile/auth/authReducer.dart';
import 'package:desktopwebmobileexample_mobile/clientLogging/ClientLoggingFake.dart';
import 'package:desktopwebmobileexample_mobile/calendarDay/CalendarDayPage.dart';
import 'package:desktopwebmobileexample_mobile/calendarDay/calendarDayEpic.dart';
import 'package:desktopwebmobileexample_mobile/calendarDay/calendarDayReducer.dart';
import 'package:desktopwebmobileexample_mobile/main.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';
import 'package:desktopwebmobileexample_mobile/nav/Nav.dart';
import 'package:desktopwebmobileexample_mobile/nav/NavImpl.dart';
import 'package:desktopwebmobileexample_mobile/remoteDb/RemoteDb.dart';
import 'package:desktopwebmobileexample_mobile/remoteDb/RemoteDbFake.dart';
import 'package:desktopwebmobileexample_mobile/ui/FadeOut.dart';
import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:redux_epics/redux_epics.dart';

Widget setup(
  Widget ui,
  NavImpl nav, //incl. nav state
  AppState preloadedState,
  Dependencies dependencies,
) {
  final store = Store<AppState>(
    /* reducer: */ (AppState state, action) {
      return new AppState(
        authState: authReducer(state.authState, action),
        calendarDayState: calendarDayReducer(state.calendarDayState, action),
      );
    },
    middleware: [
      new EpicMiddleware(
        combineEpics<dynamic>([
          calendarDayEpic(dependencies.remoteDb)
        ]),
      ),
    ],
    initialState: preloadedState,
    syncStream: false,
    distinct: true,
  );

  return StoreProvider(
    store: store,
    child: MaterialApp(
      navigatorKey: nav.nav,
      title: 'Desktop Web Mobile',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          builder: (context) => ui
        );
      },
    ),
  );
}

class MockRemoteDb extends Mock implements RemoteDb {}

void main() {

  // IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('CalendarDayPage', () {    

    testWidgets('Initial Render ReceiveEvents', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());

      Widget page = setup(
        CalendarDayPage(),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          calendarDayState: initialCalendarDayState
        ),
        Dependencies(
          nav: nav,
          auth: AuthFake(),
          remoteDb: RemoteDbFake(),
          logging: ClientLoggingFake(),
          userAgent: 'mobile-test',
        ),
      );
      
      await tester.pumpWidget(page);
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);
      expect(find.text('id1'), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(ProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
      expect(find.text('id1'), findsOneWidget);
    });

    testWidgets('Initial Render ErrorEvents', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());
      final remoteDb = MockRemoteDb();

      var answers = [
        (_) => Stream<List<Event>>.fromFuture(Future.error(StateError('Failed.'))),
        Stream.fromIterable([
          [Event(id: 'id1', name: 'name1'), ],
          [Event(id: 'id1', name: 'name1'), Event(id: 'id2', name: 'name2')],
        ]),
      ];

      when(remoteDb.listenEvents('fakeUserId'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Stream<List<Event>>;
        }
        return answer;
      });

      Widget page = setup(
        CalendarDayPage(),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          calendarDayState: initialCalendarDayState
        ),
        Dependencies(
          nav: nav,
          auth: AuthFake(),
          remoteDb: remoteDb,
          logging: ClientLoggingFake(),
          userAgent: 'mobile-test',
        ),
      );
      
      await tester.pumpWidget(page);
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);
      expect(find.text('id1'), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(ProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsOneWidget);
      expect(find.text('id1'), findsNothing);
    });

  });
}
