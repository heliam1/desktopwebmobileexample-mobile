import 'package:desktopwebmobileexample_mobile/AppState.dart';
import 'package:desktopwebmobileexample_mobile/calendarDay/calendarDayActions.dart';
import 'package:desktopwebmobileexample_mobile/calendarDay/calendarDayEpic.dart';
import 'package:desktopwebmobileexample_mobile/calendarDay/calendarDayReducer.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';
import 'package:desktopwebmobileexample_mobile/remoteDb/RemoteDb.dart';
import 'package:desktopwebmobileexample_mobile/rootReducer.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux/redux.dart';

class MockRemoteDb extends Mock implements RemoteDb {}

void main() {

  group('CalendarDayEpic', () {

    setUpAll(() {});
    setUp(() {});
    tearDown(() {});
    tearDownAll(() {});

    test('Recover from Error. Send success/receive action on listen Events success.', () async {
      final remoteDb = MockRemoteDb();

      var answers = [
        (_) => Stream<List<Event>>.fromFuture(Future.error(StateError('Failed.'))),
        Stream.fromIterable([
          [Event(id: 'id1', name: 'name1'), ],
          [Event(id: 'id1', name: 'name1'), Event(id: 'id2', name: 'name2')],
        ]),
      ];

      when(remoteDb.listenEvents('fakeUserId'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Stream<List<Event>>;
        }
        return answer;
      });

      Stream<dynamic> action$ = Stream.fromIterable([
        listenEvents('fakeUserId'),
        listenEvents('fakeUserId'),
      ]);
      EpicStore<dynamic> state$ = EpicStore<AppState>(
        Store<AppState>(
          rootReducer,
          initialState: AppState(
            calendarDayState: initialCalendarDayState,
          ),
          syncStream: false,
          distinct: true,
        ),
      );

      Stream epic$ = calendarDayEpic(remoteDb)(action$, state$);

      List<dynamic> result = await epic$.toList();

      expect(result, equals([
        calendarDayError('Bad state: Failed.'),
        receiveEvents([Event(id: 'id1', name: 'name1'), ],),
        receiveEvents([Event(id: 'id1', name: 'name1'), Event(id: 'id2', name: 'name2')],),
      ]));
    });

  });
}
