import 'package:desktopwebmobileexample_mobile/auth/resetActions.dart';
import 'package:desktopwebmobileexample_mobile/calendarDay/calendarDayActions.dart';
import 'package:desktopwebmobileexample_mobile/calendarDay/calendarDayReducer.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  group('CalendarDayReducer', () {

    test('Initial CalendarDay State', () {
      CalendarDayState expectedInitialState = CalendarDayState(
        loading: false,
        events: [],
        error: '',
      );
      CalendarDayState actualInitialState = initialCalendarDayState;
      expect(actualInitialState, expectedInitialState);
    });

    test('Listen Events', () {
      CalendarDayState currentState = new CalendarDayState(
        loading: false,
        events: [],
        error: '',
      );
      CalendarDayAction action = listenEvents('fakeUserId');
      CalendarDayState expectedNextState = new CalendarDayState(
        loading: true,
        events: [],
        error: '',
      );

      CalendarDayState actualNextState = calendarDayReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Receive Events', () {
      CalendarDayState currentState = new CalendarDayState(
        loading: true,
        events: [],
        error: '',
      );
      CalendarDayAction action = receiveEvents([
        Event(id: 'id1', name: 'name1'), Event(id: 'id2', name: 'name2')
      ]);
      CalendarDayState expectedNextState = new CalendarDayState(
        loading: false,
        events: [Event(id: 'id1', name: 'name1'), Event(id: 'id2', name: 'name2')],
        error: '',
      );

      CalendarDayState actualNextState = calendarDayReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Error Events', () {
      CalendarDayState currentState = new CalendarDayState(
        loading: true,
        events: [Event(id: 'id1', name: 'name1'), Event(id: 'id2', name: 'name2')],
        error: '',
      );
      CalendarDayAction action = calendarDayError('Failed.');
      CalendarDayState expectedNextState = new CalendarDayState(
        loading: false,
        events: [Event(id: 'id1', name: 'name1'), Event(id: 'id2', name: 'name2')],
        error: 'Failed.',
      );

      CalendarDayState actualNextState = calendarDayReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Reset', () {
      CalendarDayState currentState = new CalendarDayState(
        loading: false,
        events: [Event(id: 'id1', name: 'name1'), Event(id: 'id2', name: 'name2')],
        error: 'Failed.',
      );
      Reset action = reset();
      CalendarDayState expectedNextState = initialCalendarDayState;

      CalendarDayState actualNextState = calendarDayReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

  });
  
}
