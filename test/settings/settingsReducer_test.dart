import 'package:desktopwebmobileexample_mobile/auth/resetActions.dart';
import 'package:desktopwebmobileexample_mobile/settings/settingsActions.dart';
import 'package:desktopwebmobileexample_mobile/settings/settingsReducer.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  group('SettingsReducer', () {

    test('Initial Settings State', () {
      SettingsState expectedInitialState = SettingsState(
        loading: false,
        error: '',
      );
      SettingsState actualInitialState = initialSettingsState;
      expect(actualInitialState, expectedInitialState);
    });

    test('Log Out', () {
      SettingsState currentState = new SettingsState(
        loading: false,
        error: '',
      );
      SettingsAction action = logOut();
      SettingsState expectedNextState = new SettingsState(
        loading: true,
        error: '',
      );

      SettingsState actualNextState = settingsReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Delete Account', () {
      SettingsState currentState = new SettingsState(
        loading: false,
        error: '',
      );
      SettingsAction action = deleteAccount();
      SettingsState expectedNextState = new SettingsState(
        loading: true,
        error: '',
      );

      SettingsState actualNextState = settingsReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('SettingsSuccess', () {
      SettingsState currentState = new SettingsState(
        loading: true,
        error: '',
      );
      SettingsAction action = settingsSuccess();
      SettingsState expectedNextState = new SettingsState(
        loading: false,
        error: '',
      );

      SettingsState actualNextState = settingsReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('SettingsError', () {
      SettingsState currentState = new SettingsState(
        loading: true,
        error: '',
      );
      SettingsAction action = settingsError('Failed.');
      SettingsState expectedNextState = new SettingsState(
        loading: false,
        error: 'Failed.',
      );

      SettingsState actualNextState = settingsReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('CleanUp', () {
      SettingsState currentState = new SettingsState(
        loading: true,
        error: 'Failed.',
      );
      SettingsAction action = cleanUp();
      SettingsState expectedNextState = new SettingsState(
        loading: false,
        error: '',
      );

      SettingsState actualNextState = settingsReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Reset', () {
      SettingsState currentState = new SettingsState(
        loading: false,
        error: 'Failed.',
      );
      Reset action = reset();
      SettingsState expectedNextState = initialSettingsState;

      SettingsState actualNextState = settingsReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

  });
  
}
