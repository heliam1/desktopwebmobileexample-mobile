import 'package:desktopwebmobileexample_mobile/AppState.dart';
import 'package:desktopwebmobileexample_mobile/auth/Auth.dart';
import 'package:desktopwebmobileexample_mobile/settings/settingsActions.dart';
import 'package:desktopwebmobileexample_mobile/settings/settingsEpic.dart';
import 'package:desktopwebmobileexample_mobile/settings/settingsReducer.dart';
import 'package:desktopwebmobileexample_mobile/nav/NavFake.dart';
import 'package:desktopwebmobileexample_mobile/rootReducer.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux/redux.dart';

class MockAuth extends Mock implements Auth {}

void main() {

  group('SettingsEpic', () {

    setUpAll(() {});
    setUp(() {});
    tearDown(() {});
    tearDownAll(() {});

    test('Recover from LogOutError. Send LogOutSuccess on LogOut success.', () async {
      final nav = NavFake();
      final auth = MockAuth();

      var answers = [
        (_) => Future.error(StateError('Failed.')),
        Future.value(),
      ];

      when(auth.logOut())
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<void>;
        }
        return answer;
      });

      Stream<dynamic> action$ = Stream.fromIterable([
        logOut(),
        logOut(),
      ]);
      EpicStore<dynamic> state$ = EpicStore<AppState>(
        Store<AppState>(
          rootReducer,
          initialState: AppState(
            settingsState: initialSettingsState,
          ),
          syncStream: false,
          distinct: true,
        ),
      );

      Stream epic$ = logOutEpic(nav, auth)(action$, state$);

      List<dynamic> result = await epic$.toList();

      expect(result, equals([
        settingsError('Bad state: Failed.'),
        settingsSuccess(),
      ]));
    });

    test('Recover from DeleteAccountError. Send DeleteAccountSuccess on DeleteAccount success.', () async {
      final nav = NavFake();
      final auth = MockAuth();

      var answers = [
        (_) => Future.error(StateError('Failed.')),
        Future.value(),
      ];

      when(auth.deleteAccount())
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<void>;
        }
        return answer;
      });

      Stream<dynamic> action$ = Stream.fromIterable([
        deleteAccount(),
        deleteAccount(),
      ]);
      EpicStore<dynamic> state$ = EpicStore<AppState>(
        Store<AppState>(
          rootReducer,
          initialState: AppState(
            settingsState: initialSettingsState,
          ),
          syncStream: false,
          distinct: true,
        ),
      );

      Stream epic$ = deleteAccountEpic(nav, auth)(action$, state$);

      List<dynamic> result = await epic$.toList();

      expect(result, equals([
        settingsError('Bad state: Failed.'),
        settingsSuccess(),
      ]));
    });

  });
}
