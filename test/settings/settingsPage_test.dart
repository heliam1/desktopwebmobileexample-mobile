import 'package:desktopwebmobileexample_mobile/AppState.dart';
import 'package:desktopwebmobileexample_mobile/auth/Auth.dart';
import 'package:desktopwebmobileexample_mobile/auth/authActions.dart';
import 'package:desktopwebmobileexample_mobile/auth/authReducer.dart';
import 'package:desktopwebmobileexample_mobile/clientLogging/ClientLoggingFake.dart';
import 'package:desktopwebmobileexample_mobile/settings/SettingsPage.dart';
import 'package:desktopwebmobileexample_mobile/settings/settingsEpic.dart';
import 'package:desktopwebmobileexample_mobile/settings/settingsReducer.dart';
import 'package:desktopwebmobileexample_mobile/main.dart';
import 'package:desktopwebmobileexample_mobile/nav/Nav.dart';
import 'package:desktopwebmobileexample_mobile/nav/NavImpl.dart';
import 'package:desktopwebmobileexample_mobile/remoteDb/RemoteDbFake.dart';
import 'package:desktopwebmobileexample_mobile/ui/FadeOut.dart';
import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:redux_epics/redux_epics.dart';

Widget setup(
  Widget ui,
  NavImpl nav, //incl. nav state
  AppState preloadedState,
  Dependencies dependencies,
) {
  final store = Store<AppState>(
    /* reducer: */ (AppState state, action) {
      return new AppState(
        authState: authReducer(state.authState, action),
        settingsState: settingsReducer(state.settingsState, action),
      );
    },
    middleware: [
      new EpicMiddleware(
        combineEpics<dynamic>([
          logOutEpic(nav, dependencies.auth),
          deleteAccountEpic(nav, dependencies.auth),
        ]),
      ),
    ],
    initialState: preloadedState,
    syncStream: false,
    distinct: true,
  );

  return StoreProvider(
    store: store,
    child: MaterialApp(
      navigatorKey: nav.nav,
      title: 'Desktop Web Mobile',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          builder: (context) => ui
        );
      },
    ),
  );
}

class AuthMock extends Mock implements Auth {}

void main() {

  // IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('SettingsPage', () {    

    testWidgets('Initial Render -> LogOut Success', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());
      final auth = AuthMock();
      final remoteDb = RemoteDbFake();
      final logging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      var answers = [
        Future.value(),
      ];

      when(auth.logOut())
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        return answer;
      });

      Widget page = setup(
        SettingsPage(),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          settingsState: initialSettingsState
        ),
        Dependencies(
          nav: nav,
          auth: auth,
          remoteDb: remoteDb,
          logging: logging,
          userAgent: userAgent,
        ),
      );
      
      await tester.pumpWidget(page);
      await tester.pumpAndSettle();

      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
      expect(find.text('Log out'), findsOneWidget);
      expect(find.text('Deactivate account'), findsOneWidget);
      await tester.tap(find.text('Log out'));

      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpWidget(Container());
      await tester.pumpAndSettle();
      // This is causing issue https://github.com/flutter/flutter/issues/24166
      // onDispose: (store) {
      //  store.dispatch(cleanUp());
      // },
      // Pending timers:
      // Timer (duration: 0:00:00.000000, periodic: false), created:
      // #0      new FakeTimer._ (package:fake_async/fake_async.dart:284:41)
    });
    
    testWidgets('Initial Render -> LogOut Error then Success', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());
      final auth = AuthMock();
      final remoteDb = RemoteDbFake();
      final logging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      var answers = [
        (_) => Future.error(StateError('Failed.')),
        Future.value(),
      ];

      when(auth.logOut())
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<void>;
        }
        return answer;
      });

      Widget page = setup(
        SettingsPage(),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          settingsState: initialSettingsState
        ),
        Dependencies(
          nav: nav,
          auth: auth,
          remoteDb: remoteDb,
          logging: logging,
          userAgent: userAgent,
        ),
      );
      
      await tester.pumpWidget(page);
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
      expect(find.text('Log out'), findsOneWidget);
      expect(find.text('Deactivate account'), findsOneWidget);
      await tester.tap(find.text('Log out'));

      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsOneWidget);

      await tester.tap(find.text('Log out'));

      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
      
      await tester.pumpWidget(Container());
      await tester.pumpAndSettle();
    });

    testWidgets('Initial Render -> DeleteAccount Success', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());
      final auth = AuthMock();
      final remoteDb = RemoteDbFake();
      final logging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      var answers = [
        Future.value(),
      ];

      when(auth.deleteAccount())
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        return answer;
      });

      Widget page = setup(
        SettingsPage(),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          settingsState: initialSettingsState
        ),
        Dependencies(
          nav: nav,
          auth: auth,
          remoteDb: remoteDb,
          logging: logging,
          userAgent: userAgent,
        ),
      );
      
      await tester.pumpWidget(page);
      await tester.pumpAndSettle();

      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
      expect(find.text('Log out'), findsOneWidget);
      expect(find.text('Deactivate account'), findsOneWidget);
      await tester.tap(find.text('Deactivate account'));

      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpWidget(Container());
      await tester.pumpAndSettle();
    });

    testWidgets('Initial Render -> DeleteAccount Error then Success', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());
      final auth = AuthMock();
      final remoteDb = RemoteDbFake();
      final logging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      var answers = [
        (_) => Future.error(StateError('Failed.')),
        Future.value(),
      ];

      when(auth.deleteAccount())
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<void>;
        }
        return answer;
      });

      Widget page = setup(
        SettingsPage(),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          settingsState: initialSettingsState
        ),
        Dependencies(
          nav: nav,
          auth: auth,
          remoteDb: remoteDb,
          logging: logging,
          userAgent: userAgent,
        ),
      );
      
      await tester.pumpWidget(page);
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
      expect(find.text('Log out'), findsOneWidget);
      expect(find.text('Deactivate account'), findsOneWidget);
      await tester.tap(find.text('Deactivate account'));

      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsOneWidget);

      await tester.tap(find.text('Deactivate account'));

      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
      
      await tester.pumpWidget(Container());
      await tester.pumpAndSettle();
    });

  });
}
