import 'package:desktopwebmobileexample_mobile/AppState.dart';
import 'package:desktopwebmobileexample_mobile/auth/authActions.dart';
import 'package:desktopwebmobileexample_mobile/event/eventActions.dart';
import 'package:desktopwebmobileexample_mobile/event/eventEpic.dart';
import 'package:desktopwebmobileexample_mobile/event/eventReducer.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';
import 'package:desktopwebmobileexample_mobile/nav/NavFake.dart';
import 'package:desktopwebmobileexample_mobile/remoteDb/RemoteDb.dart';
import 'package:desktopwebmobileexample_mobile/rootReducer.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:redux/redux.dart';

class RemoteDbMock extends Mock implements RemoteDb {}

void main() {

  group('EventEpic', () {

    setUpAll(() {});
    setUp(() {});
    tearDown(() {});
    tearDownAll(() {});
    
    test('Recover from GetEvent Error. Send GetEventSuccess on remoteDb.getEvent success.', () async {
      final remoteDb = RemoteDbMock();

      var answers = [
        (_) => Future<Event>.error(StateError('Failed.')),
        Future.value(Event(id: 'fakeEventId', name: 'fakeEventName',)),
      ];

      when(remoteDb.getEvent('fakeUserId', 'fakeEventId'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<Event>;
        }
        return answer;
      });

      Stream<dynamic> action$ = Stream.fromIterable([
        getEvent('fakeUserId', 'fakeEventId'),
        getEvent('fakeUserId', 'fakeEventId'),
      ]);
      EpicStore<dynamic> state$ = EpicStore<AppState>(
        Store<AppState>(
          rootReducer,
          initialState: AppState(
            eventState: initialEventState,
            authState: AuthState(
              authenticated: true,
              initialized: true,
              userId: 'fakeUserId',
            ),
          ),
          syncStream: false,
          distinct: true,
        ),
      );

      Stream epic$ = getEventEpic(remoteDb)(action$, state$);

      List<dynamic> result = await epic$.toList();

      expect(result, equals([
        getEventError('Bad state: Failed.'),
        getEventSuccess(Event(id: 'fakeEventId', name: 'fakeEventName')),
      ]));
    });

    test('Recover from DeleteEvent Error. Send DeleteEventSuccess on remoteDb.deleteEvent success.', () async {
      final remoteDb = RemoteDbMock();
      final nav = NavFake();

      var answers = [
        (_) => Future.error(StateError('Failed.')),
        Future.value(),
      ];

      when(remoteDb.deleteEvent('fakeUserId', 'fakeEventId'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<void>;
        }
        return answer;
      });

      Stream<dynamic> action$ = Stream.fromIterable([
        deleteEvent('fakeUserId', 'fakeEventId'),
        deleteEvent('fakeUserId', 'fakeEventId'),
      ]);
      EpicStore<dynamic> state$ = EpicStore<AppState>(
        Store<AppState>(
          rootReducer,
          initialState: AppState(
            eventState: initialEventState,
            authState: AuthState(
              authenticated: true,
              initialized: true,
              userId: 'fakeUserId',
            ),
          ),
          syncStream: false,
          distinct: true,
        ),
      );

      Stream epic$ = deleteEventEpic(remoteDb, nav)(action$, state$);

      List<dynamic> result = await epic$.toList();

      expect(result, equals([
        deleteEventError('Bad state: Failed.'),
        deleteEventSuccess(),
      ]));
    });

    test('Recover from SaveEvent Error. Send SaveEventSuccess on remoteDb.saveEvent success.', () async {
      final remoteDb = RemoteDbMock();
      final nav = NavFake();

      var answers = [
        (_) => Future.error(StateError('Failed.')),
        Future.value(),
      ];

      when(remoteDb.saveEvent('fakeUserId', Event(id: 'fakeEventId', name: 'fakeEventName',)))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<void>;
        }
        return answer;
      });

      Stream<dynamic> action$ = Stream.fromIterable([
        saveEvent(Event(id: 'fakeEventId', name: 'fakeEventName',)),
        saveEvent(Event(id: 'fakeEventId', name: 'fakeEventName',)),
      ]);
      EpicStore<dynamic> state$ = EpicStore<AppState>(
        Store<AppState>(
          rootReducer,
          initialState: AppState(
            eventState: initialEventState,
            authState: AuthState(
              authenticated: true,
              initialized: true,
              userId: 'fakeUserId',
            ),
          ),
          syncStream: false,
          distinct: true,
        ),
      );

      Stream epic$ = saveEventEpic(remoteDb, nav)(action$, state$);

      List<dynamic> result = await epic$.toList();

      expect(result, equals([
        saveEventError('Bad state: Failed.'),
        saveEventSuccess(),
      ]));
    });

  });
}
