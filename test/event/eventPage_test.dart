import 'package:desktopwebmobileexample_mobile/AppState.dart';
import 'package:desktopwebmobileexample_mobile/auth/AuthFake.dart';
import 'package:desktopwebmobileexample_mobile/auth/authActions.dart';
import 'package:desktopwebmobileexample_mobile/clientLogging/ClientLoggingFake.dart';
import 'package:desktopwebmobileexample_mobile/event/EventPage.dart';
import 'package:desktopwebmobileexample_mobile/event/eventEpic.dart';
import 'package:desktopwebmobileexample_mobile/event/eventReducer.dart';
import 'package:desktopwebmobileexample_mobile/main.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';
import 'package:desktopwebmobileexample_mobile/nav/Nav.dart';
import 'package:desktopwebmobileexample_mobile/nav/NavImpl.dart';
import 'package:desktopwebmobileexample_mobile/remoteDb/RemoteDb.dart';
import 'package:desktopwebmobileexample_mobile/ui/FadeOut.dart';
import 'package:flutter/material.dart';
import 'package:mockito/mockito.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:redux_epics/redux_epics.dart';

Widget setup(
  Widget ui,
  NavImpl nav, //incl. nav state
  AppState preloadedState,
  Dependencies dependencies,
) {
  final store = Store<AppState>(
    /* reducer: */ (AppState state, action) {
      return new AppState(
        authState: AuthState(
          authenticated: true,
          initialized: true,
          userId: 'fakeUserId',
        ),
        eventState: eventReducer(state.eventState, action),
      );
    },
    middleware: [
      new EpicMiddleware(
        combineEpics<dynamic>([
          saveEventEpic(dependencies.remoteDb, nav),
          getEventEpic(dependencies.remoteDb),
          deleteEventEpic(dependencies.remoteDb, nav),
        ]),
      ),
    ],
    initialState: preloadedState,
    syncStream: false,
    distinct: true,
  );

  return StoreProvider(
    store: store,
    child: MaterialApp(
      navigatorKey: nav.nav,
      title: 'Desktop Web Mobile',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      onGenerateRoute: (RouteSettings settings) {
        return MaterialPageRoute(
          builder: (context) => ui
        );
      },
    ),
  );
}

class RemoteDbMock extends Mock implements RemoteDb {}

void main() {

  // IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('EventPage', () {    

    testWidgets('Initial Render -> NewEvent', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());
      final auth = AuthFake();
      final remoteDb = RemoteDbMock();
      final logging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      Widget page = setup(
        EventPage('newEvent'),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          eventState: initialEventState
        ),
        Dependencies(
          nav: nav,
          auth: auth,
          remoteDb: remoteDb,
          logging: logging,
          userAgent: userAgent,
        ),
      );
      
      await tester.pumpWidget(page);
      // The newEvent action is dispatched and received before the widget is even built.
      // So I think we are not receiving even 1 frame of loading.
      // expect(find.byType(CircularProgressIndicator), findsOneWidget);
      // expect(find.byType(FadeOut), findsNothing);
      await tester.pumpAndSettle();

      expect(find.byIcon(Icons.delete_forever), findsNothing);
      expect(find.text('newEvent'), findsOneWidget);
      expect(find.text(''), findsOneWidget);
      expect(find.text('Save'), findsOneWidget);
      
      await tester.pumpWidget(Container());
      await tester.pumpAndSettle();
    });
    
    testWidgets('Initial Render -> GetEvent', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());
      final auth = AuthFake();
      final remoteDb = RemoteDbMock();
      final logging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      var answers = [
        Future.value(Event(id: 'fakeEventId', name: 'fakeEventName')),
      ];

      when(remoteDb.getEvent('fakeUserId', 'fakeEventId'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        return answer;
      });

      Widget page = setup(
        EventPage('fakeEventId'),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          eventState: initialEventState
        ),
        Dependencies(
          nav: nav,
          auth: auth,
          remoteDb: remoteDb,
          logging: logging,
          userAgent: userAgent,
        ),
      );
      
      await tester.pumpWidget(page);
      expect(find.byIcon(Icons.delete_forever), findsOneWidget);
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);
      expect(find.text('loading'), findsOneWidget); // findsNWidgets(2)
      expect(find.widgetWithText(TextField, 'loading'), findsOneWidget);
      expect(find.text('Save'), findsOneWidget);
      
      await tester.pumpAndSettle();
      expect(find.byIcon(Icons.delete_forever), findsOneWidget);
      expect(find.text('fakeEventId'), findsOneWidget);
      expect(find.text('fakeEventName'), findsOneWidget);
      expect(find.text('Save'), findsOneWidget);
      
      await tester.pumpWidget(Container());
      await tester.pumpAndSettle();
    });

    testWidgets('Initial Name Change', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());
      final auth = AuthFake();
      final remoteDb = RemoteDbMock();
      final logging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      Widget page = setup(
        EventPage('newEvent'),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          eventState: initialEventState
        ),
        Dependencies(
          nav: nav,
          auth: auth,
          remoteDb: remoteDb,
          logging: logging,
          userAgent: userAgent,
        ),
      );
      
      await tester.pumpWidget(page);
      await tester.pumpAndSettle();

      expect(find.byIcon(Icons.delete_forever), findsNothing);
      expect(find.text('newEvent'), findsOneWidget);
      expect(find.text(''), findsOneWidget);
      expect(find.text('Save'), findsOneWidget);

      await tester.enterText(find.widgetWithText(TextField, ''), 'name');
      expect(find.widgetWithText(TextField, 'name'), findsOneWidget);
      
      await tester.pumpAndSettle();
      expect(find.widgetWithText(TextField, 'name'), findsOneWidget);
      
      await tester.pumpWidget(Container());
      await tester.pumpAndSettle();
    });

    // save event fails then succeeds
    testWidgets('Initial Render -> GetEvent -> SaveEvent Fail -> SaveEvent Succeed', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());
      final auth = AuthFake();
      final remoteDb = RemoteDbMock();
      final logging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      var answers = [
        Future<Event>.value(Event(id: 'fakeEventId', name: 'fakeEventName')),
        (_) => Future<void>.error(StateError('Failed.')),
        Future<void>.value()
      ];

      when(remoteDb.getEvent('fakeUserId', 'fakeEventId'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<void>;
        }
        return answer;
      });

      when(remoteDb.saveEvent('fakeUserId', Event(id: 'fakeEventId', name: 'fakeEventName')))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<void>;
        }
        return answer;
      });

      Widget page = setup(
        EventPage('fakeEventId'),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          eventState: initialEventState
        ),
        Dependencies(
          nav: nav,
          auth: auth,
          remoteDb: remoteDb,
          logging: logging,
          userAgent: userAgent,
        ),
      );
        
      await tester.pumpWidget(page);
      expect(find.byIcon(Icons.delete_forever), findsOneWidget);
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);
      expect(find.text('loading'), findsOneWidget); // findsNWidgets(2)
      expect(find.widgetWithText(TextField, 'loading'), findsOneWidget);
      expect(find.text('Save'), findsOneWidget);
        
      await tester.pumpAndSettle();
      expect(find.byIcon(Icons.delete_forever), findsOneWidget);
      expect(find.text('fakeEventId'), findsOneWidget);
      expect(find.text('fakeEventName'), findsOneWidget);
      expect(find.text('Save'), findsOneWidget);

      await tester.tap(find.text('Save'));
      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsOneWidget);

      await tester.tap(find.text('Save'));
      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
        
      await tester.pumpWidget(Container());
      await tester.pumpAndSettle();
    });

    // delete event fails then succeeds

    testWidgets('Initial Render -> GetEvent -> DeleteEvent Fail -> DeleteEvent Succeed', (WidgetTester tester) async {

      final Nav nav = NavImpl(GlobalKey<NavigatorState>());
      final auth = AuthFake();
      final remoteDb = RemoteDbMock();
      final logging = ClientLoggingFake();
      final userAgent = 'mobile-test';

      var answers = [
        Future<Event>.value(Event(id: 'fakeEventId', name: 'fakeEventName')),
        (_) => Future<void>.error(StateError('Failed.')),
        Future<void>.value()
      ];

      when(remoteDb.getEvent('fakeUserId', 'fakeEventId'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<void>;
        }
        return answer;
      });

      when(remoteDb.deleteEvent('fakeUserId', 'fakeEventId'))
      .thenAnswer((_) {
        var answer = answers.removeAt(0);
        if (answer is Function(dynamic)) {
          return answer(_) as Future<void>;
        }
        return answer;
      });

      Widget page = setup(
        EventPage('fakeEventId'),
        nav,
        AppState(
          authState: AuthState(
            authenticated: true,
            initialized: true,
            userId: 'fakeUserId',
          ),
          eventState: initialEventState
        ),
        Dependencies(
          nav: nav,
          auth: auth,
          remoteDb: remoteDb,
          logging: logging,
          userAgent: userAgent,
        ),
      );
      
      await tester.pumpWidget(page);
      expect(find.byIcon(Icons.delete_forever), findsOneWidget);
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);
      expect(find.text('loading'), findsOneWidget); // findsNWidgets(2)
      expect(find.widgetWithText(TextField, 'loading'), findsOneWidget);
      expect(find.text('Save'), findsOneWidget);
      
      await tester.pumpAndSettle();
      expect(find.byIcon(Icons.delete_forever), findsOneWidget);
      expect(find.text('fakeEventId'), findsOneWidget);
      expect(find.text('fakeEventName'), findsOneWidget);
      expect(find.text('Save'), findsOneWidget);

      await tester.tap(find.byIcon(Icons.delete_forever));
      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsOneWidget);

      await tester.tap(find.byIcon(Icons.delete_forever));
      await tester.pump();
      expect(find.byType(CircularProgressIndicator), findsOneWidget);
      expect(find.byType(FadeOut), findsNothing);

      await tester.pumpAndSettle();
      expect(find.byType(CircularProgressIndicator), findsNothing);
      expect(find.byType(FadeOut), findsNothing);
      
      await tester.pumpWidget(Container());
      await tester.pumpAndSettle();
    });

    // THEN DONE. WOOOO. DEV VS TEST VS PROD. CICD.

  });
}
