import 'package:desktopwebmobileexample_mobile/auth/resetActions.dart';
import 'package:desktopwebmobileexample_mobile/event/eventActions.dart';
import 'package:desktopwebmobileexample_mobile/event/eventReducer.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {

  group('EventReducer', () {

    test('Initial Event State', () {
      EventState expectedInitialState = EventState(
        loading: true,
        event: Event(
          id: 'loading',
          name: 'loading',
        ),
        error: '',
        ignoreChange: false,
      );
      EventState actualInitialState = initialEventState;
      expect(actualInitialState, expectedInitialState);
    });

    test('New event', () {
      EventState currentState = new EventState(
        loading: false,
        event: Event(
          id: 'loading',
          name: 'loading',
        ),
        error: '',
      );
      EventAction action = newEvent();
      EventState expectedNextState = new EventState(
        loading: false,
        event: Event(
          id: 'newEvent',
          name: '',
        ),
        error: '',
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Name changed', () {
      EventState currentState = new EventState(
        loading: false,
        event: Event(
          id: 'newEvent',
          name: '',
        ),
        error: '',
      );
      EventAction action = nameChanged('Name!');
      EventState expectedNextState = new EventState(
        loading: false,
        event: Event(
          id: 'newEvent',
          name: 'Name!',
        ),
        error: '',
        ignoreChange: true,
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Save Event', () {
      EventState currentState = new EventState(
        loading: false,
        event: Event(
          id: 'newEvent',
          name: 'Name!',
        ),
        error: '',
      );
      EventAction action = saveEvent(
        Event(
          id: 'newEvent',
          name: 'Name!',
        ),
      );
      EventState expectedNextState = new EventState(
        loading: true,
        event: Event(
          id: 'newEvent',
          name: 'Name!',
        ),
        error: '',
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Save Success', () {
      EventState currentState = new EventState(
        loading: true,
        event: Event(
          id: 'newEvent',
          name: 'Name!',
        ),
        error: '',
      );
      EventAction action = saveEventSuccess();

      EventState expectedNextState = new EventState(
        loading: false,
        event: Event(
          id: 'newEvent',
          name: 'Name!',
        ),
        error: '',
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Save Error', () {
      EventState currentState = new EventState(
        loading: true,
        event: Event(
          id: 'newEvent',
          name: 'Name!',
        ),
        error: '',
      );
      EventAction action = saveEventError('Failed.');

      EventState expectedNextState = new EventState(
        loading: false,
        event: Event(
          id: 'newEvent',
          name: 'Name!',
        ),
        error: 'Failed.',
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Get Event', () {
      EventState currentState = new EventState(
        loading: false,
        event: Event( id: "loading", name: "loading"),
        error: '',
      );
      EventAction action = getEvent('fakeUserId', 'fakeEventId');

      EventState expectedNextState = new EventState(
        loading: true,
        event: Event( id: "loading", name: "loading"),
        error: '',
        ignoreChange: false,
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Get Event Success', () {
      EventState currentState = new EventState(
        loading: true,
        event: Event( id: "loading", name: "loading"),
        error: '',
      );
      EventAction action = getEventSuccess(
        Event(id: 'id1', name: 'name1',),
      );

      EventState expectedNextState = new EventState(
        loading: false,
        event: Event(id: 'id1', name: 'name1',),
        error: '',
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Get Event Error', () {
      EventState currentState = new EventState(
        loading: true,
        event: Event(id: "loading", name: "loading"),
        error: '',
      );
      EventAction action = getEventError('Failed.');

      EventState expectedNextState = new EventState(
        loading: false,
        event: Event(id: 'error', name: 'error',),
        error: 'Failed.',
        ignoreChange: false,
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Delete Event', () {
      EventState currentState = new EventState(
        loading: false,
        event: Event(id: 'id1', name: 'name1'),
        error: '',
      );
      EventAction action = deleteEvent('fakeUserId', 'fakeEventId');

      EventState expectedNextState = new EventState(
        loading: true,
        event: Event(id: 'id1', name: 'name1'),
        error: '',
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Delete Event Success', () {
      EventState currentState = new EventState(
        loading: true,
        event: Event(id: 'id1', name: 'name1'),
        error: '',
      );
      EventAction action = deleteEventSuccess();

      EventState expectedNextState = new EventState(
        loading: false,
        event: Event(id: 'id1', name: 'name1'),
        error: '',
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Delete Event Error', () {
      EventState currentState = new EventState(
        loading: true,
        event: Event(id: 'id1', name: 'name1'),
        error: '',
      );
      EventAction action = deleteEventError('Failed.');

      EventState expectedNextState = new EventState(
        loading: false,
        event: Event(id: 'id1', name: 'name1'),
        error: 'Failed.',
      );

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Clean up', () {
      EventState currentState = new EventState(
        loading: false,
        event: Event(
          id: 'id1',
          name: 'name1',
        ),
        error: 'Failed.',
      );
      EventAction action = cleanUp();
      EventState expectedNextState = initialEventState;

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

    test('Reset', () {
      EventState currentState = new EventState(
        loading: false,
        event: Event(
          id: 'id1',
          name: 'name1',
        ),
        error: 'Failed.',
      );
      Reset action = reset();
      EventState expectedNextState = initialEventState;

      EventState actualNextState = eventReducer(
        currentState,
        action,
      );
      expect(actualNextState, equals(expectedNextState));
    });

  });
  
}
