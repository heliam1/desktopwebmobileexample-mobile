# Building upon the Ubuntu or Debian Node OS with Node.js and its dependencies already installed
# FROM node:12.19.0
FROM cirrusci/flutter

# Copy the required directories into the image
# Copying root project directory to root container directory . (excl. files/folders listed in dockerignore file)
COPY / /opt/atlassian/pipelines/agent/build

ENV CI CI
ENV FIREBASE_TOKEN_PROD 1//0g2din1g32Lj4CgYIARAAGBASNwF-L9IrBF5PuVaGPDsZNpkWlVEnF8m-_izwqTaFVKSPm5zPz7qoXZT8GS_oXvRop7itYYrdUtM
ENV FIREBASE_TOKEN_DEV 1//0geoKEQT3dWMSCgYIARAAGBASNwF-L9IrPk_EZMGn2VNAOB1lgJ3BZ0siW26ehUf0g6jd4inzHaBMNjV1J7S_shXxdlzIxQ9ezoA
ENV FIREBASE_TOKEN_TEST 1//0ggZpLoqfHfbGCgYIARAAGBASNwF-L9Ir_IHBa_59xbwY-ivrFRAfQItltPK4YVrdVTld4rz7pkBsDgYTXwL-kqXZjcVCduzcBYw
ENV TYPE_TEST_SA service_account
ENV PROJECT_ID_TEST_SA desktop-web-mobile-test
ENV PRIVATE_KEY_ID_TEST_SA f40ffaf541bfcbcca5216812b400cbfeec4b6c60

ENV PRIVATE_KEY_TEST_SA -----BEGIN PRIVATE KEY-----\\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQClspev7Dj0cLZj\\nWmZ4FtzjD7lKXOExV3bpzj7VpWbPRkTH2J3+W54hLMvE/pOz9DPSzCz0ZYs6OOCz\\nYEAKl6sb2Bm10FImL3+KiSbzDCBRERhTv6ebeKFi/T5C++SDFwu0gQRTRu3bqK+G\\nKzFqXTRJJy3SCTXS2rTGWEBwcGPf9twyJJCgRZX9CxqARdTuKuBL/I5bbKs2TVcX\\nsvXIja6hnlk3xRRfsbIDCVpsJMO4Fhg1XveOXd8Cb/xiajou9POndWtmKIx8qTiU\\nzJyT+StDMTOwUPCkCZH+Wsvyn36iJSdtUUO1nUI9rKAnYEjm2hYuEey8l5zbkR9P\\nBH4RkbsnAgMBAAECggEATUV4dKuKMgBAJhwZziYHU4JZLAMdZ3e89jLe3Z45UC8a\\nU0JDB8Ps/daYnmEEO3cGFedujXivUXMF4tDffLEXNDB2aPHh5XngFhh4x/5fMb7x\\no9isTpYWWQX1W6KBgdqk9EP4Yuegc//5/u00qcAWaojxVtJBvfeNNzQftO/Mu7vw\\n+XWMFds3PQfoXl0TUc9BUrqKru22DKv2Pd4xhnqkDR/obW4qw1Wop6fXDv3++hmB\\nklcquUFbCYEi7Lmyzt6PIfP1RskoOqW6kMeBs0XvBDNxxaIORLmsWAFmG9+EOeOV\\nmwcCZwdhUlYzK5AYpvoyOJP6zQy44zU8+ga+kyW4XQKBgQDR+tkY+6gCLv5FfX4x\\nVOUkPvTaxKR4vYprbI21iCyees1wo4yDe4vidtLaOdS7x2jENgdr+mpcTIU3n1Ew\\nPvKxlfEwrkFYHNGyuumsZBrIQMNUdB708jTN5XZZPR9lZkdwXZYRmHfIYD5ekee2\\nJjqUziNyBttEzrt1KuPXEyLrYwKBgQDKAz1/4cSqGsrYN3m4rKVnMwe7SGkGrFNf\\n2VQjWhSTKasjsigwUR+s2bxCf47mQQ9mfDNydlpcRvjIX5liUbtWC7YEG3BjD6sv\\neH6w7DzNPqRzuqlnlCmvI2cO9TJdqiZVUVZkW8+naL6o90QYFMsaLCkI43uJhnP0\\nKx/DenMWbQKBgBw9bmErx0az50KVRBmCX2bmiKu7cptCcpFMAir963JM9cBGXql4\\nPjHw5R6wVGoEwZRR5+GfxhovuGLorFSewqkbj1qKtQMSrSkqdi7XL7rq6to628b5\\npBxk1eveipBqlwUHsR0gsktilkDFwrH+pjZJGv9E6MxyOUfiws20JygdAoGAcYtI\\nsexQAdx1Tvo/bFF91neUTOvyPFH3utQIuqYTEnRFQRcOB7pRN3JaonXX+AtxgIUm\\n1Zs4Rs2NX/J74AtYZJDIiZ5ymPthmJj9fKwLsMEY5gNw3Esm8s3PS1T+Li9IxIOn\\nL1l7afkx0P7KQ07MqbVQizJxaWctHXxvqgvVTDECgYAe3RVScaOJ3w1ub+y864TD\\nTTj5N7Cwp3Ta7nkiae9ZolFqyCe+z45Mu92dOtcURsYd3GX9d8wvjeI5EkKkG4+j\\nWbQ641XjFTRb+ROkDf5cAXMNwaSlp0g7q9ufM9bm4XqDqUDzVl4/RXIthh7C365v\\ncO9iWhk2fhaHyXRAoihY1Q==\\n-----END PRIVATE KEY-----\\n
ENV CLIENT_ID_TEST_SA 109985605012346506855
ENV AUTH_URI_TEST_SA https://accounts.google.com/o/oauth2/auth
ENV TOKEN_URI_TEST_SA https://oauth2.googleapis.com/token
ENV AUTH_PROVIDER_X509_CERT_URL_TEST_SA https://www.googleapis.com/oauth2/v1/certs
ENV CLIENT_X509_CERT_URL_TEST_SA https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-wacgy%40desktop-web-mobile-test.iam.gserviceaccount.com
ENV TYPE_PROD_SA service_account
ENV PROJECT_ID_PROD_SA desktop-web-mobile
ENV PRIVATE_KEY_ID_PROD_SA 0d2658ce9ede55dc2eb710498e499d78819b578f
ENV PRIVATE_KEY_PROD_SA -----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCuuMc2zPWarsSD\\nr55pF/ya2S6pceehdecqzXKZM4XA0BDXOfqCsplb2ie1Q690xeuBJF3Od3DT10Kp\\nJn7Ii7dqXOuXWBs8k98uE3YHSDf8hFGdadRu+tG0aVkR5BcwCeYKx4F34roQSEE2\\nn5PjseK0mID0SLHdMwvUoE5HVavtWWUYtIsS1sK5Bw6E+bypnQG8ZycHeqe+7Gn1\\nvJHCQt3lplbj87936POWGa+LygkZ5blWPao5dfvU3Kgy4emp5fa0l4rb+IZYPEV3\\nzdUlFtGCtpnQfijpG9GbWg2dRRKZfwHmGCJAvqtSoLEw/J2eYiALdSs9s0ZrZJHc\\nnvMF/ioXAgMBAAECggEAHGLh3sni1zwwqjJkmq9ud1v0pEvgtY85vO7/ANvpjZ9g\\nNQ9y3zwUBWPGksEJ5v9m2Sbm5K3sE0bESkX6UCu+41VwYzjJyAhAOLSpttopWmnG\\n9ruka1ltq5F4CpuTltbs3HB4XNlloFKU4cKAXPxgGDY0sTuERitObz+SKC3Hjnp0\\nZE0ckT/O4QBF36LXKS3r9uUkUqs8YethskozgJCkEcss62m/YEtjScBj2UDdmdt9\\n5qzHEywgI0In6E3Bir8c25hPwzYLJ25At4st+Y/zgLczjH9O9xTkMkGRb2ke7lwt\\nCJGmoBpn+0i+9nGShh0wC/OwoUmTi6dRoMQLjzugGQKBgQDVnswPg0X7DM5eqkUY\\nD5WgxU5QYP3PDLARIYTiLMI8iHs3BVzlCBL55TK+K5s3L1fkz+ZMX19t9lR3tlhq\\nW3NBe6v7uYTdzMc0X0Yj7qGgfe6WpW8MLEA01L3G7QZldAuD1E21j/JFjoAow14Z\\nktJYG0Dvi/jhAihm2BwpltG/2QKBgQDRYm4Luik8HlnSTGOGre9CjhLh7mAdFVj0\\nowcQQVweE93qqOWTZnPgidoHffSK8oWpSoR6SMEPpS6Qety26xX4g6eFMTHnST6a\\n+fk/ck2XO/3ZzA5FiqvOzmHPe5bAGtVf+TnlbfIPJVox0e01wYoggpXbz30XCWbv\\nItAskdnzbwKBgQCfSyjTsIowqN4CzWl0hbqIBlldqtXY3Zc1Tw9uMP4ucX+gCcm5\\nqAmuevEZyh6WSiF6qGUNMllRAE0Ab+MCfGs3u2cF8NNZKeip7xm4lavEp+OQRpDG\\nPQEr5DAX9Iu+f4hOp7PC3jJFF/wNaZHz8/4SYVBM0M2nzBoghqqJgMOsEQKBgEcG\\no5u4x3oMNZ3fqbcq00l5VjBroxDah/VcQ+4grbXKOXH7Xvz0OQSG3n3CHSzw/aQi\\nkfPFMDA1cSkzdCFcd3R4fF/zAyGYt/LsLr9175oIkKDL1l+CnasmWQl/u/Boac8H\\nhLst43l9yy2xL00X3NoQUfNhW+Zvmwotff5Qb2VjAoGAB/AkW9TVAwO2ekY96Cho\\nBrtscJCWE78TC+P4O9KTgU2wUTRh/CDhFcHe3OtLKxASq83Y/g0c59EH/8iCydZK\\nKY9dQIajOOR4A5WeXcbGO5ZVMZhyugAn2EDiOLL0koWgaO/w2wDROo401Ml5iKK+\\n3RflGn5RXUAvRsv/gtc0AFQ=\\n-----END PRIVATE KEY-----\\n
ENV CLIENT_EMAIL_PROD_SA firebase-adminsdk-lsqr1@desktop-web-mobile.iam.gserviceaccount.com
ENV CLIENT_ID_PROD_SA 103039439475125611457
ENV AUTH_URI_PROD_SA https://accounts.google.com/o/oauth2/auth
ENV TOKEN_URI_PROD_SA https://oauth2.googleapis.com/token
ENV AUTH_PROVIDER_X509_CERT_URL_PROD_SA https://www.googleapis.com/oauth2/v1/certs
ENV CLIENT_X509_CERT_URL_PROD_SA https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-lsqr1%40desktop-web-mobile.iam.gserviceaccount.com

# Key Properties. Notice the passwords are aliased
ENV STORE_PASSWORD Lx4_WHEN_DEBUGGING_NEED_MANUALLY_SET_THIS_ON_LOCAL_CONTAINER
ENV STORE_KEY_PASSWORD Lx4_WHEN_DEBUGGING_NEED_MANUALLY_SET_THIS_ON_LOCAL_CONTAINER
ENV STORE_KEY_ALIAS_TEST mobile_key_test
ENV STORE_KEY_ALIAS_PROD mobile_key
ENV STORE_FILE ../../desktopwebmobileexample_key.jks
ENV SUFFIX_TEST _test

# Change to working directory
WORKDIR /opt/atlassian/pipelines/agent/build

# Finally build the image this dockerfile defines by
# PS C:\Users\Engineer\VSCodeProjects\desktopwebmobileexample_mobile> docker build --memory=4g --memory-swap=4g -t liamjtb/desktopwebmobile_cicd .







#
# If installing Flutter FROM base Ubuntu or Node
# (Atm we use from a CI company that has already built Flutter, but kept their dockerfile private)
#

# git clone https://github.com/flutter/flutter.git
# export PATH="$PATH:`pwd`/flutter/bin"
# flutter doctor
# flutter precache
# apt update && apt install android-sdk
# TODO
# flutter doctor
# flutter doctor --android-licenses
# apt update && apt install android-sdk

#
# Other Docker things
#

# Install dependencies
# RUN npm install
# Expose API port to the outside
# EXPOSE 80
# Launch application
# CMD ["npm","start"]

# --entrypoint=/bin/bash
# If you need to pass environment variables into your container you can use the -e switch, for example adding -e "VAR1=hello" -e "VAR2=world" to the above command before -it, or use a file --env-file=env-vars.txt
# docker run --env-file=env-vars.txt liamjtb/desktopwebmobile_cicd 
# docker run
# -e "CI"
# liamjtb/desktopwebmobile_cicd 
