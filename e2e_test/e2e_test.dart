import 'package:desktopwebmobileexample_mobile/main.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:integration_test/integration_test.dart';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'access_token.dart';
import 'testCredentials.dart';

String userEmail = '';

Future<void> signUp(WidgetTester tester) async {
  // tester.resetTestTextInput();
  await tester.enterText(find.widgetWithText(TextField, 'Email'), testCredentials['email']);
  await tester.enterText(find.widgetWithText(TextField, 'Password'), testCredentials['pw']);
  await tester.tap(find.text('Sign Up'));
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
}

/*Future<void> logIn(WidgetTester tester) async {
  print('Clearing email');
  TextField emailTextField = find.byKey(ValueKey('home_email')).first.evaluate().first.widget;
  String email = emailTextField.controller.text;
  await tester.tap(find.byKey(ValueKey('home_email')));
  for (int i = 0; i < email.length; i++) {
    if (emailTextField.controller.selection.base.offset <= email.length)
      await tester.sendKeyEvent(LogicalKeyboardKey.arrowRight);
  }
  for (int i = 0; i < email.length; i++) {
    await tester.sendKeyEvent(LogicalKeyboardKey.backspace);
  }

  print('Clearing pw');
  TextField pwTextField = find.byKey(ValueKey('home_password')).first.evaluate().first.widget;
  String pw = pwTextField.controller.text;
  
  await tester.tap(find.byKey(ValueKey('home_password')));
  for (int i = 0; i < pw.length; i++) {
    // add check for cursor position. 
    print('base offset: ${pwTextField.controller.selection.base.offset}');
    print('pw length: ${pw.length}');
    if (pwTextField.controller.selection.base.offset <= pw.length)
      await tester.sendKeyEvent(LogicalKeyboardKey.arrowRight);
  }
  for (int i = 0; i < pw.length; i++) {
    await tester.sendKeyEvent(LogicalKeyboardKey.backspace);
  }

  print('Entering email and pw');
  await tester.enterText(find.byKey(ValueKey('home_email')), testCredentials['email']);
  await tester.enterText(find.byKey(ValueKey('home_password')), testCredentials['pw']);

  print('Clicking Login');
  await tester.tap(find.text('Login'));
  await tester.pumpAndSettle(Duration(milliseconds: 5000));
}*/

Future<void> logIn1(WidgetTester tester) async {
  print('Clicking Login');
  await tester.tap(find.text('Login'));
  await tester.pumpAndSettle(Duration(milliseconds: 5000));
}

Future<void> logIn2(WidgetTester tester) async {
  print('Entering email and pw');
  await tester.enterText(find.byKey(ValueKey('home_email')), testCredentials['email']);
  await tester.enterText(find.byKey(ValueKey('home_password')), testCredentials['pw']);

  print('Clicking Login');
  await tester.tap(find.text('Login'));
  await tester.pumpAndSettle(Duration(milliseconds: 5000));
}

Future<void> addAnEvent(WidgetTester tester, String eventNameKey) async {
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
  await tester.tap(find.byType(FloatingActionButton));
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
    
  await tester.enterText(find.byKey(ValueKey('event_name')), 'My event name$eventNameKey');
  await tester.tap(find.byKey(ValueKey('event_save')));
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
}

Future<void> clickAnEvent(WidgetTester tester, String eventNameKey) async {
  print('clicking event $eventNameKey.');
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
  await tester.tap(find.text(eventNameKey));
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
  print('clicked event $eventNameKey.');
}

Future<void> modifyAnEvent(WidgetTester tester, String eventNameKey) async {
  print('Modifying event $eventNameKey.');
  await tester.pumpAndSettle(Duration(milliseconds: 1000));

  // get event name widget
  // Returns a list of elements. Elements are Widgets which are instantiated.
  // In Flutter there is a Widget Tree and an Element Tree. Widgets of the same
  // type in the Widget Tree are equivalent. Elements of the same type in the 
  // Element Tree are not equivalent - they could have different text contents
  // or size or children for example.
  TextField nameTextField = find.byType(TextField).first.evaluate().first.widget;

  // get event name input value
  String name = nameTextField.controller.text;
  
  // click event name
  await tester.tap(find.byType(TextField));

  // input arrow right length times
  for (int i = 0; i < name.length; i++) {
    await tester.sendKeyEvent(LogicalKeyboardKey.arrowRight);
  }

  // input Backspace length times
  for (int i = 0; i < name.length; i++) {
    await tester.sendKeyEvent(LogicalKeyboardKey.backspace);
  }

  // input eventNameKey
  await tester.enterText(find.byType(TextField), eventNameKey);

  print('modified event text: ${nameTextField.controller.text}');

  // press save
  await tester.tap(find.byKey(ValueKey('event_save')));

  await tester.pumpAndSettle(Duration(milliseconds: 1000));
}

Future<void> deleteAnEvent(WidgetTester tester) async {
  print('Deleting event.');
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
  await tester.tap(find.byKey(ValueKey('event_delete')));
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
  print('Deleted event.');
}

Future<void> navigateToSettings(WidgetTester tester) async {
  print('Navigating to settings page.');
  // user navigates to settings
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
  await tester.tap(find.byKey(ValueKey('nav_profile')));
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
}

Future<void> signOut(WidgetTester tester) async {
  print('Signing out.');
  await tester.tap(find.byKey(ValueKey('settings_sign_out')));
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
}

Future<void> deleteAccount(WidgetTester tester) async {
  print('Deleting account.');
  await tester.tap(find.byKey(ValueKey('settings_delete_account')));
  await tester.pumpAndSettle(Duration(milliseconds: 1000));
}

// This cleans up remoteDb and Auth, not the app/browser
Future<void> cleanUp(String userEmail) async {
  if (userEmail != '') {
    try {
      Map<String, String> envVars = Platform.environment;
      var projectId;
      if (envVars['GCP_PROJECT'] != null) projectId = envVars['GCP_PROJECT'];
      if (envVars['GCLOUD_PROJECT'] != null) projectId = envVars['GCLOUD_PROJECT'];

      if (projectId == null) {
        print('Authenticating with a service account since not on Google environment.');
          
        // We use API key when running e2e test locally. Would pref to use OAuth via service account though.

        // var serviceAccount = await require('./desktop-web-mobile-dev-firebase-adminsdk-1765k-3fcd3a3eb1.json');
        // We need to authenticate with a service account if running the test locally.
        // admin.initializeApp({
        //  projectId: config.projectId,
        //  credential: admin.credential.cert(serviceAccount),
        //  databaseURL: "https://desktop-web-mobile-dev.firebaseio.com"
        //});

        String accessToken = await getAccessToken();
        print('Sending clean up request');
        print('User email: ' + userEmail);

        print('project id: ${Firebase.apps[0].options.projectId}');

        Response response = await http.post(
          'https://us-central1-${Firebase.apps[0].options.projectId}.cloudfunctions.net/testCleanUpE2e', // url
          // method specified already
          headers: {
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer ' + accessToken,
          },
          body: jsonEncode({ // data
            'userEmail': userEmail,
          }),
        );

        print('Clean up status: ${response.statusCode}');

        // if running e2e test in cloud function
        // we don't include service account in git repo, we use application default credentials if running from cloud function
        // app is already initialized
      } else {
        throw new UnimplementedError("Currently this code does not run on a Google Environment.");
        //const auth = new AuthImpl();
        //const remoteDb = new RemoteDbImpl();
      
        //console.log(`Finding user id for user email ${userEmail}.`)
        //const userId = await remoteDb.findUserIdForEmail(userEmail);
        //console.log(`User id: ${userId}.`)
        //await remoteDb.deleteUser(userId);
        //console.log('Deleted user from remote database.')
        //await auth.deleteUser(userId);
        //console.log('Deleted user from authentication.')
      }
    } catch (err) {
      print('Couldn\'t clean up environment due to:');
      print(err);
      throw new StateError("Error cleaning up. The environment may need to be manually cleaned.");
    }
  }
  print('Clean up succeeded.');
}

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  tearDown(() async {
    await cleanUp(userEmail);
  });

  testWidgets("E2E Test", (WidgetTester tester) async {
    await tester.pumpWidget(await createApp());
    await tester.pumpAndSettle();

    // TODO: this should probably be configurable, and injected into signUp.
    userEmail = '';
  
    try {
      await signUp(tester);
      userEmail = testCredentials['email'];
      await logIn1(tester);
      for (int i = 0; i < 10; i++) {
        await addAnEvent(tester, i.toString());
      }

      await clickAnEvent(tester, "My event name9");
      await modifyAnEvent(tester, "My modified event name");
      await clickAnEvent(tester, "My event name7");
      await deleteAnEvent(tester);

      await navigateToSettings(tester);
      await signOut(tester);

      await tester.pumpAndSettle(Duration(milliseconds: 1000));

      await logIn2(tester);
      await navigateToSettings(tester);
      await deleteAccount(tester);
      /*if (config.userAgent === 'chrome') {
        await tester.pumpAndSettle(Duration(milliseconds: 5000));
        await download(tester);
      }*/
      
    } catch (err) {
      print(err);
      await cleanUp(userEmail);
      print('Clean up success');
      throw (err);
    } finally {
      // close app if need be
    }

    print('Should be complete');
  });
}
