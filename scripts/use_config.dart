import 'dart:async';
import 'dart:io';

Future<List<FileSystemEntity>> dirContents(Directory dir) {
  var files = <FileSystemEntity>[];
  var completer = Completer<List<FileSystemEntity>>();
  var lister = dir.list(recursive: false);
  lister.listen (
    (file) => files.add(file),
    // should also register onError
    onDone:   () => completer.complete(files)
  );
  return completer.future;
}

main(List<String> args) async {
  
  if (args[0] == null) {
    print("Please use arg --prod or --test or --dev");
    throw ArgumentError("Please use arg --prod or --test or --dev");
  }

  var dirName = args[0];
  // e.g. C:\Users\Engineer\VSCodeProjects\desktopwebmobileexample_mobile
  Directory current = Directory.current;

  Directory configDirectory;
  
  if (dirName == "--dev") {
    configDirectory = Directory(current.path + '/config/dev');
  } else if (dirName == "--test") {
    configDirectory = Directory(current.path + '/config/test');
  } else if (dirName == "--prod") {
    configDirectory = Directory(current.path + '/config/prod');
  } else {
    print("Please use arg --prod or --test or --dev");
    throw ArgumentError("Please use arg --prod or --test or --dev");
  }
  
  await copyGoogleServices(current, configDirectory);
  await copyKeyProperties(current, configDirectory);
}

Future<void> copyGoogleServices(Directory current, Directory configDirectory) async {
  var files = await dirContents(configDirectory);
  String googleServicesPath;

  for (var file in files) {

    var path = file.path;

    if (path.contains('google-services')) {
      googleServicesPath = path;
    }
  }

  if (googleServicesPath == null) {
    throw ArgumentError("Please ensure there is a config file i.e. google-services.json and keystore.jks");
  }

  var googleServicesFile = File(googleServicesPath);

  String googleServicesContents;

  if (await googleServicesFile.exists()) {
    // Read file
    googleServicesContents = await googleServicesFile.readAsString();
  
    var googleServicesLines = googleServicesContents.split('\n');

    // Build file
    googleServicesContents = '';
    for (var line in googleServicesLines) {
      googleServicesContents = googleServicesContents + line + '\n';
    }

    var googleServicesAndroidDir = Directory(current.path + '/android/app/');
    var googleServicesAndroidFile = File(googleServicesAndroidDir.path + '/google-services.json');
    await googleServicesAndroidFile.writeAsString(googleServicesContents);
  } else {
    throw ArgumentError("A config file did not exist i.e. google-services.json");
  }
}

Future<void> copyKeyProperties(Directory current, Directory configDirectory) async {
  var files = await dirContents(configDirectory);
  String keyPropertiesPath;

  for (var file in files) {

    var path = file.path;

    if (path.contains('key.properties')) {
      keyPropertiesPath = path;
    }
  }

  String keyPropertiesContents = '';

  if (keyPropertiesPath == null) {
    Map<String, String> env = Platform.environment;

    keyPropertiesContents += 'storePassword=${env['STORE_PASSWORD']}\n';
    keyPropertiesContents += 'keyPassword=${env['STORE_KEY_PASSWORD']}\n';
    if (configDirectory.path.contains('test')) {
      keyPropertiesContents += 'keyAlias=${env['STORE_KEY_ALIAS_TEST']}\n';
    } else {
      keyPropertiesContents += 'keyAlias=${env['STORE_KEY_ALIAS_PROD']}\n';
    }
    keyPropertiesContents += 'storeFile=${env['STORE_FILE']}\n';
    if (configDirectory.path.contains('test')) {
      keyPropertiesContents += 'suffix=${env['SUFFIX_TEST']}\n';
    } else {
      keyPropertiesContents += 'suffix=\n';
    }


  } else {
    var keyPropertiesFile = File(keyPropertiesPath);

    if (await keyPropertiesFile.exists()) {
      // Read file
      keyPropertiesContents = await keyPropertiesFile.readAsString();

      var keyPropertiesLines = keyPropertiesContents.split('\n');

      // Build file
      keyPropertiesContents = '';
      for (var line in keyPropertiesLines) {
        keyPropertiesContents = keyPropertiesContents + line + '\n';
      }
    } else {
      throw ArgumentError("A config file did not exist key.properties");
    }
  }

  var keyPropertiesAndroidDir = Directory(current.path + '/android/');
  var keyPropertiesAndroidFile = File(keyPropertiesAndroidDir.path + '/key.properties');
  await keyPropertiesAndroidFile.writeAsString(keyPropertiesContents);
}

// key_store.jks is already copied via bitbucket script from base 64 encoded encrypted env variable
