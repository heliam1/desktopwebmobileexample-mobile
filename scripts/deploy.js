const publishAab = require("publish-aab-google-play");
 
(async () => {
  try {
    await publishAab.publish({
      keyFile: "./scripts/pc-api-5110906416711108437-319-59bbf997ff2a.json",
      packageName: "app.web.desktop_web_mobile", // url is desktop-web-mobile.web.app
      aabFile: "build/app/outputs/bundle/release/app-release.aab",
      track: "production" // alpha beta production
    });
    console.log("Success");
  } catch (error) {
    console.error(error);
  }
})();

/*
const jwt = require("jsonwebtoken")
const config = require("../android/app/google-services.json")
const axios = require("axios");
const FormData = require('form-data');
const fs = require('fs');

(async () => {
  try {
    if (config.project_info.project_id !== 'desktop-web-mobile') {
      throw new Error('This script should only be run for prod deployment.');
    }

    let credentials;

    if (process.env.CI) {
      credentials = {
        private_key_id: process.env.PLAY_PRIVATE_KEY_ID_PROD_SA,
        private_key: process.env.PLAY_PRIVATE_KEY_PROD_SA.replace(/\\n/g, '\n'),
        client_email: process.env.PLAY_CLIENT_EMAIL_PROD_SA,
      };
    } else {
      credentials = require('./pc-api-5110906416711108437-319-59bbf997ff2a.json');
    }

    // Create JWT - header, claim set, signature.
    const jwtHeader = {
      "alg": "RS256",
      "typ": "JWT",
      "kid": credentials.private_key_id,
    };

    // https://console.developers.google.com/apis/dashboard?project=pc-api-5110906416711108437-319
    const nowInSeconds = Math.floor(new Date().valueOf() / 1000);

    const jwtClaimSet = {
      "iss": credentials.client_email, // service account email address
      "scope": "https://www.googleapis.com/auth/androidpublisher", // scope of google play API
      "aud": "https://oauth2.googleapis.com/token",
      "exp": nowInSeconds + 3600, // seconds since UTC for which assertion should expire. Max 1 hour.
      "iat": nowInSeconds, // time the assertion was issued. seconds since utc.
    };

    // either {Base64url encoded header}.{Base64url encoded claim set} or utf8 equivalent as input.
    // create a byte array (the signature) from ^ . Sign the utf8 array version of the input with the private key with SHA256withRSA
    // at this point you will have header{}.claims{}.signaturebytes. Base 64 encode this so it ends up as header.claims.signature

    // TODO: if an issue occurs, I suspect it is here
    const token = jwt.sign(
      jwtClaimSet,
      credentials.private_key,
      {
        algorithm: "RS256",
        // expiresIn: "1h", // > node scripts/deploy.js > Error: Bad "options.expiresIn" option the payload already has an "exp" property.
        header: jwtHeader
      }, // choose correct signing algorithm, SHA256withRSA
    );

    console.log(`Token: ${token}`);
    //const decoded = jwt.verify(token, credentials.private_key);
    // console.log(`decoded: ${decoded}`);

    // send request with JWT to get access token
    const res = await axios({
      method: 'post',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      url: `https://oauth2.googleapis.com/token`,
      params: {
        grant_type: "urn:ietf:params:oauth:grant-type:jwt-bearer",
        assertion: token
      }
    });

    // Handle the JSON response that the Authorization Server returns.
    // If the response includes an access token, you can use the access token to call a Google API
      //{
      //"access_token": "1/8xbJqaOZXSUZbHLl5EOtu1pxz3fmmetKx9W8CV4t79M",
      //"scope": "https://www.googleapis.com/auth/prediction"
      //"token_type": "Bearer",
      //"expires_in": 3600
      //}
    console.log(`response:\nstatus:-${res.status}\naccess_token-${res.data.access_token}\nscope-${res.data.scope}\ntoken_type-${res.data.token_type}\nexpires_in-${res.data.expires_in}`);
    console.log(`response:\nerror-${res.error}\nerror_description-${res.error_description}`);
  
    // Use the access_token to use the publishing API
    // https://developers.google.com/android-publisher/api-ref/rest
    // https://androidpublisher.googleapis.com/$discovery/rest?version=v3

    // https://developers.google.com/android-publisher/edits
    // https://developers.google.com/android-publisher/api-ref/rest#rest-resource:-v3.edits
    // https://developers.google.com/android-publisher/api-ref/rest/v3/edits/insert

    // Create an edit

    const packageName = "app.web.desktop_web_mobile";
    const editRes = await axios({
      method: 'post',
      headers: {
        'Authorization': `Bearer ${res.data.access_token}`,
        'Content-Type': 'application/json',
      },
      url: `https://androidpublisher.googleapis.com/androidpublisher/v3/applications/${packageName}/edits`,
    });

    console.log(`editRes:\nstatus:${editRes.status}`);

    // Notice that in Google Documentation a Resource doesn't need to be named in the JSON object if it is top level. I.e. no data.AppEdit.id , just data.id,
    const appEdit = {
      id: editRes.data.id,
      expiryTimeSeconds: editRes.data.expiryTimeSeconds,
    };

    // Upload a new .aab (.apk discouraged) to the edit.
    // Upload documentation https://developers.google.com/android-publisher/upload

    const form = new FormData();
    // for fs, it depends on the dir where the script is run from. I.e. we are doing node scripts/deploy.js, not pushd scripts && node deploy.js && popd
    form.append("file", fs.createReadStream('build/app/outputs/bundle/release/app-release.aab'));

    // TODO: read the tab we found, figure everything out
    // has the npm lib, googleapis-node lib
    // 

    const aabRes = await axios({
      method: 'post',
      headers: {
        'Authorization': `Bearer ${res.data.access_token}`,
        // This is handled by. https://stackoverflow.com/a/59177066/8599411
        ...form.getHeaders(), // "Content-Type": "multipart/form-data; boundary=-------------------0123456789" 
        // TODO: confirm "Content-Length": number_of_bytes_in_entire_request_body     exists
      },
      url: `https://androidpublisher.googleapis.com/upload/androidpublisher/v3/applications/${packageName}/edits/${appEdit.id}/bundles`,
      'maxContentLength': Infinity,
      'maxBodyLength': Infinity,
      params: {
        upload_type: "multipart",
      },
      data: form
    }); 

    console.log(`aabRes:\nstatus:${aabRes.status}`);

    //return axios
    //.post(url, form_data, request_config);

    // Update store listing.  
    // Edits.apk:upload // add a new APK to a track.
    // Edit.listings:update | patch // update store listing. Update creates new localized listing. Patch modifies existing store listing.

  } catch (err) {
    console.log(`Error: ${err}. Message: ${err.message}`);
  }
})();
*/
