const fs = require('fs');

(async () => {
  var args = process.argv.slice(2);
  
  var serviceAccount;

  if (args[0] === "--prod") {
    // create js object
    serviceAccount = {
      "type": process.env.TYPE_PROD_SA,
      "project_id": process.env.PROJECT_ID_PROD_SA,
      "private_key_id": process.env.PRIVATE_KEY_ID_PROD_SA,
      "private_key": process.env.PRIVATE_KEY_PROD_SA.replace(/\\n/g, '\n'),
      "client_email": process.env.CLIENT_EMAIL_PROD_SA,
      "client_id": process.env.CLIENT_ID_PROD_SA,
      "auth_uri": process.env.AUTH_URI_PROD_SA,
      "token_uri": process.env.TOKEN_URI_PROD_SA,
      "auth_provider_x509_cert_url": process.env.AUTH_PROVIDER_X509_CERT_URL_PROD_SA,
      "client_x509_cert_url": process.env.CLIENT_X509_CERT_URL_PROD_SA
    };
  } else if (args[0] === "--test") {
    serviceAccount = {
      "type": process.env.TYPE_TEST_SA,
      "project_id": process.env.PROJECT_ID_TEST_SA,
      "private_key_id": process.env.PRIVATE_KEY_ID_TEST_SA,
      "private_key": process.env.PRIVATE_KEY_TEST_SA.replace(/\\n/g, '\n'),
      "client_email": process.env.CLIENT_EMAIL_TEST_SA,
      "client_id": process.env.CLIENT_ID_TEST_SA,
      "auth_uri": process.env.AUTH_URI_TEST_SA,
      "token_uri": process.env.TOKEN_URI_TEST_SA,
      "auth_provider_x509_cert_url": process.env.AUTH_PROVIDER_X509_CERT_URL_TEST_SA,
      "client_x509_cert_url": process.env.CLIENT_X509_CERT_URL_TEST_SA
    };
  } else if (args[0] === "--dev") {
    serviceAccount = require("./desktop-web-mobile-dev-firebase-adminsdk-1765k-3fcd3a3eb1.json");
  } else {
    throw Error('Please specify whether creating the dev, test or prod service account via --dev or --test or --prod');
  }

  // write js object to json format
  fs.writeFileSync('scripts/serviceAccount.json', JSON.stringify(serviceAccount), 'utf8');
})();
