import 'package:desktopwebmobileexample_mobile/auth/authReducer.dart';

import 'AppState.dart';
import 'home/homeReducer.dart';
import 'counter/counterReducer.dart';
import 'calendarDay/calendarDayReducer.dart';
import 'event/eventReducer.dart';
import 'settings/settingsReducer.dart';

AppState rootReducer(AppState state, action) {
  return new AppState(
    authState: authReducer(state.authState, action),
    homeState: homeReducer(state.homeState, action),
    counterState: counterReducer(state.counterState, action),
    calendarDayState: calendarDayReducer(state.calendarDayState, action),
    eventState: eventReducer(state.eventState, action),
    settingsState: settingsReducer(state.settingsState, action),
  );
} 
