import 'dart:async';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';

abstract class RemoteDb {
  Stream<int> listenCount();
  Future<void> increment(int count);
  Stream<List<Event>> listenEvents(String userId);
  Future<void> stopListeningEvents();
  Future<Event> getEvent(String userId, String eventId);
  Future<void> saveEvent(String userId, Event event);
  Future<void> deleteEvent(String userId, String eventId);
  String createNewEventKey(String userId);
  String createNewRepeatKey(String userId);
  String createNewConstraintKey(String userId);
  void dispose();
}
