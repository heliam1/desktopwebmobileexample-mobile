import 'dart:async';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';
import 'package:desktopwebmobileexample_mobile/remoteDb/RemoteDb.dart';
import 'package:rxdart/rxdart.dart';

class RemoteDbFake implements RemoteDb {

  Stream<int> listenCount() {
    return Stream.fromIterable([
      0,
      1,
      2,
      3,
    ]);
  }

  Future<void> increment(int count) async {

  }

  Stream<List<Event>> listenEvents(String userId) {
    return Stream.fromIterable([
      [Event(id: 'id1', name: 'name1'), ],
      [Event(id: 'id1', name: 'name1'), Event(id: 'id2', name: 'name2')],
    ]);
  }
  
  Future<void> stopListeningEvents() async {

  }

  Future<Event> getEvent(String userId, String eventId) {
    return Future.value(new Event(id: '1', name: '1'));
  }

  Future<void> saveEvent(String userId, Event event) async {
  }
  
  Future<void> deleteEvent(String userId, String eventId) async {
  }

  String createNewEventKey(String userId) {
    return '1';
  }

  String createNewRepeatKey(String userId) {
    return '1';
  }

  String createNewConstraintKey(String userId) {
    return '1';
  }

  void dispose() async {
  }
}
