import 'dart:async';

import 'package:desktopwebmobileexample_mobile/models/Event.dart';
import 'package:rxdart/rxdart.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'RemoteDb.dart';

class RemoteDbFirebase implements RemoteDb {
  FirebaseFirestore _firestore;

  StreamSubscription<DocumentSnapshot> counterSub;
  ReplaySubject<int> counterStreamController;

  StreamSubscription<QuerySnapshot> eventsSub;
  ReplaySubject<List<Event>> eventsStreamController;

  RemoteDbFirebase(FirebaseApp app) {
    _firestore = FirebaseFirestore.instanceFor(app: app);
  }

  Stream<int> listenCount() {
    if (this.counterStreamController == null) {
      this.counterStreamController = new ReplaySubject(maxSize: 1);
    }
    if (this.counterSub == null) {
      counterSub =
      _firestore.collection('count').doc('count').snapshots().listen((ds) {
        counterStreamController.add(ds.data()['count']);
      });
    }
    return this.counterStreamController.stream;
  }

  Future<void> increment(int count) {
    count = count + 1;

    return _firestore
    .collection('count')
    .doc('count')
    .set({ 'count': count })
    .catchError((error) => print(error));
  }

  Stream<List<Event>> listenEvents(String userId) {
    if (this.eventsStreamController == null) {
      this.eventsStreamController = new ReplaySubject(maxSize: 1);
    }
    if (this.eventsSub == null) {
      eventsSub =
      _firestore
      .collection('users')
      .doc(userId)
      .collection('events')
      .orderBy('name')
      .snapshots()
      .listen((qs) {
        List<Event> events = [];

        for (QueryDocumentSnapshot qds in qs.docs) {
          events.add(
            new Event(
              id: qds.id,
              name: qds.data()['name']
            )
          );
        }

        eventsStreamController.add(events);
      });
    }
    return this.eventsStreamController.stream;
  }
  
  Future<void> stopListeningEvents() async {
    if (this.eventsSub != null) {
      eventsSub.cancel();
      eventsSub = null;
      await this.eventsStreamController.done;
      this.eventsStreamController = null;
    }
  }

  Future<Event> getEvent(String userId, String eventId) {
    return this._firestore
    .collection('users')
    .doc(userId)
    .collection('events')
    .doc(eventId)
    .get()
    .then((DocumentSnapshot value) {
      if (!value.exists) {
        throw new StateError('Document does not exist');
      } else {
        return new Event(
          id: value.id,
          name: value.data()['name'],
        );
      }
    });
  }

  Future<void> saveEvent(String userId, Event event) {
    return this._firestore
    .collection('users')
    .doc(userId)
    .collection('events')
    .doc(event.id == 'newEvent' ? this.createNewEventKey(userId) : event.id)
    .set({
      'name': event.name
    });
  }
  
  Future<void> deleteEvent(String userId, String eventId) {
    return this._firestore
    .collection('users')
    .doc(userId)
    .collection('events')
    .doc(eventId)
    .delete();
  }

  String createNewEventKey(String userId) {
    return this._firestore
    .collection('users')
    .doc(userId)
    .collection('events')
    .doc()
    .id;
  }

  String createNewRepeatKey(String userId) {
    return this._firestore
    .collection('users')
    .doc(userId)
    .collection('repeats')
    .doc()
    .id;
  }

  String createNewConstraintKey(String userId) {
    return this._firestore
    .collection('users')
    .doc(userId)
    .collection('constraints')
    .doc()
    .id;
  }

  void dispose() async {
    print('Dispose of remote');
    if (this.counterSub != null) {
      counterSub.cancel();
      counterSub = null;
      await this.counterStreamController.done;
      this.counterStreamController = null;
    }
    if (this.eventsSub != null) {
      eventsSub.cancel();
      eventsSub = null;
      await this.eventsStreamController.done;
      this.eventsStreamController = null;
    }
  }
}
