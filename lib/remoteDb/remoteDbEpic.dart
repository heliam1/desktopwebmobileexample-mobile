import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../AppState.dart';
import 'RemoteDbFirebase.dart';

import 'remoteDbActions.dart';

Epic<AppState> remoteDbEpic(RemoteDbFirebase remoteDb) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<DisposeRemoteDb>()
    .map((action) => remoteDb.dispose())
    .map((_) => disposeRemoteDbSuccess())
    .cast<RemoteDbAction>()
    .onErrorResume((error) => Stream.fromIterable([disposeRemoteDbError(error.toString())]));
  };
}
