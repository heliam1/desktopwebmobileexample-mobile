import '../reduxToolkit.dart';

class RemoteDbAction extends Action {}

class DisposeRemoteDb extends RemoteDbAction {}

class DisposeRemoteDbSuccess extends RemoteDbAction {}

class DisposeRemoteDbError extends RemoteDbAction {
  final String error;
  DisposeRemoteDbError(this.error);
}

DisposeRemoteDb disposeRemoteDb() => new DisposeRemoteDb();
DisposeRemoteDbSuccess disposeRemoteDbSuccess() => new DisposeRemoteDbSuccess();
DisposeRemoteDbError disposeRemoteDbError(String error) => new DisposeRemoteDbError(error);
