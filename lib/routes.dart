import 'package:flutter/material.dart';

import 'home/HomePage.dart';
import 'calendarDay/CalendarDayPage.dart';
import 'counter/CounterPage.dart';
import 'event/EventPage.dart';
import 'settings/SettingsPage.dart';

bool first = true;

Route<dynamic> routes(RouteSettings settings) {
  // In web we use our own loading/splash/launch page here
  // When checking if the the app is initialized with a refererence to 
  // store auth state.
  // On Flutter, we aren't really able to do this.
  // Instead, it depends on a platform by platform basis from what I can tell
  // https://flutter.dev/docs/development/ui/advanced/splash-screen

  

  var uri = Uri.parse(settings.name);
  if (uri.pathSegments.length == 2
  && uri.pathSegments.first == 'details') {
    return MaterialPageRoute(builder: (context) => CounterPage(/*id: id*/));
  } else if (uri.pathSegments.length == 1 && uri.pathSegments.first == 'calendarDay') {
    return MaterialPageRoute(builder: (context) => CalendarDayPage());
  } else if (uri.pathSegments.length == 2 && uri.pathSegments.first == 'event') {
    return MaterialPageRoute(builder: (context) => EventPage(uri.pathSegments[1]));
  } else if (uri.pathSegments.length == 1 && uri.pathSegments.first == 'settings') {
    return MaterialPageRoute(builder: (context) => SettingsPage());
  }

  print('Pushing: ${settings.name}');
  // Handle '/'
  if (settings.name == '/') {
    return MaterialPageRoute(builder: (context) => HomePage());
  }

  return MaterialPageRoute(builder: (context) => CounterPage());
  
  // TODO: 404 return MaterialPageRoute(builder: (context) => UnknownScreen());
}