import 'package:desktopwebmobileexample_mobile/auth/PrivateComponent.dart';
import 'package:desktopwebmobileexample_mobile/nav/navActions.dart';
import 'package:desktopwebmobileexample_mobile/ui/FadeOut.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';

import '../nav/NavigationBar.dart';
import '../AppState.dart';
import 'eventActions.dart';

class _ViewModel {
  bool loading;
  Event event;
  String error;
  VoidCallback onBackPressed;
  VoidCallback onDeletePressed;
  Function(String) onNameChanged;
  VoidCallback onFabPressed;

  _ViewModel(
    this.loading,
    this.event,
    this.error,
    this.onBackPressed,
    this.onDeletePressed,
    this.onNameChanged,
    this.onFabPressed,
  );
}

class EventPage extends StatelessWidget {
  final String _eventId;

  EventPage(
    this._eventId
  );

  @override
  Widget build(BuildContext context) {

    return StoreConnector<AppState, _ViewModel>(
      distinct: true,
      ignoreChange: (state) {
        return state.eventState.ignoreChange;
      },
      onInit: (store) {
        if (this._eventId == 'newEvent') {
          print('onInit dispatch newEvent');
          store.dispatch(newEvent());
        } else {
          print('onInit dispatch getEvent');
          store.dispatch(
            getEvent(
              store.state.authState.userId,
              this._eventId
            )
          );
        }
      },
      onDispose: (store) {
        store.dispatch(cleanUp());
      },
      converter: (store) {
        return _ViewModel(
          store.state.eventState.loading,
          store.state.eventState.event,
          store.state.eventState.error,
          () { store.dispatch(pop()); },
          () { 
            store.dispatch(
              deleteEvent(
                store.state.authState.userId,
                store.state.eventState.event.id,
              )
            );
          },
          (string) {
            store.dispatch(nameChanged(string));
          },
          () { store.dispatch(saveEvent(store.state.eventState.event)); },
        );

      },
      builder: (context, vm) {
        print('builder: ${vm.loading} ${vm.event} ${vm.error}');
        List<Widget> children = [];

        children.add(eventView(context, vm));
        if (vm.loading) { children.add(loadingView()); }
        if (vm.error != '') { children.add(errorView(vm.error)); }
        
        return Scaffold(
          body: SafeArea(
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    child: SingleChildScrollView(
                      child: Container(
                        height: MediaQuery.of(context).size.height - 50 - MediaQuery.of(context).padding.top,
                        width: MediaQuery.of(context).size.width,
                        child: Stack(
                          children: children
                        ),
                      ),
                    ),
                  ),
                ),
                NavigationBar(
                  2,
                ),
              ],
            ),
          ),
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          floatingActionButton: Padding(
            padding: EdgeInsets.only(
              bottom: 60,
            ),
            child: FloatingActionButton.extended(
              key: ValueKey('event_save'),
              onPressed: vm.onFabPressed,
              label: Text('Save'),
              icon: Icon(Icons.check),
            ),
          ),
        );
      },
    );
  }

  Widget eventView(BuildContext context, _ViewModel vm) {
    return PrivateComponent(
      child: Column(
        children: [
          Row(
            children: [
              GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: vm.onBackPressed,
                child: Padding(
                  padding: EdgeInsets.only(left: 2),
                  child: Container(
                    height: 50,
                    width: 50,
                    child: Icon(
                      Icons.arrow_back,
                      color: const Color(0xff5d99c6),
                    ),
                  ),
                ),
              ),
              Text(this._eventId),
              Visibility(
                visible: this._eventId != 'newEvent',
                child: MaterialButton(
                  key: ValueKey('event_delete'),
                  child: Icon(Icons.delete_forever),
                  onPressed: vm.onDeletePressed
                ),
              ),
            ],
          ),
          TextField(
            key: ValueKey('event_name'),
            onChanged: vm.onNameChanged,
            controller: TextEditingController.fromValue(
              TextEditingValue(
                text: vm.event.name,
                //TODO: this cursor behaviour on tap is non optimal
                selection: TextSelection.fromPosition(
                  TextPosition(
                    offset: vm.event.name.length
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget loadingView() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget errorView(String error) {
    return Center(
      child: FadeOut(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          child: Text(error),
        ),
      ),
    );
  }
}
