import 'package:desktopwebmobileexample_mobile/reduxToolkit.dart';
import 'package:desktopwebmobileexample_mobile/auth/resetActions.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';

import 'eventActions.dart';

final EventState initialEventState = new EventState(
  loading: true,
  event: new Event(
    id: 'loading',
    name: 'loading'
  ),
  error: '',
);

EventState eventReducer(EventState state, Action action) {
  if (action is NewEvent) {
    return state.copyWith(
      loading: false,
      event: new Event(
        id: 'newEvent',
        name: ''
      ),
      error: '',
    );
  } else if (action is NameChanged) {
    return state.copyWith(
      event: state.event.copyWith(name: action.name),
      ignoreChange: true,
    );
  } else if (action is SaveEvent) {
    return state.copyWith(
      loading: true,
      event: action.event,
      error: '',
    );
  } else if (action is SaveEventSuccess) {
    return state.copyWith(
      loading: false,
      error: '',
    );
  } else if (action is SaveEventError) {
    return state.copyWith(
      loading: false,
      error: action.error,
    );
  } else if (action is GetEvent) {
    return state.copyWith(
      loading: true,
      error: '',
    );
  } else if (action is GetEventSuccess) {
    return state.copyWith(
      loading: false,
      event: Event(
        id: action.event.id,
        name: action.event.name,
      ),
      error: '',
    );
  } else if (action is GetEventError) {
    return state.copyWith(
      loading: false,
      event: Event(
        id: 'error',
        name: 'error',
      ),
      error: action.error,
    );
  } else if (action is DeleteEvent) {
    return state.copyWith(
      loading: true,
      error: '',
    );
  } else if (action is DeleteEventSuccess) {
    return state.copyWith(
      loading: false,
      error: '',
    );
  } else if (action is DeleteEventError) {
    return state.copyWith(
      loading: false,
      error: action.error,
    );
  } else if (action is CleanUp) { 
    return initialEventState;
  } else if (action is Reset) {
    return initialEventState;
  } else {
    return state;
  }
}