import 'package:desktopwebmobileexample_mobile/nav/Nav.dart';
import 'package:desktopwebmobileexample_mobile/remoteDb/RemoteDb.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../AppState.dart';
import 'eventActions.dart';

// EpicStore<dynamic> store:
// A stripped-down Redux [Store]. Removes unsupported [Store] methods.
// Due to the way streams are implemented with Dart, it's impossible to
// perform store.dispatch from within an [Epic] or observe the store
// directly.

Epic<AppState> saveEventEpic(RemoteDb remoteDb, Nav nav) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<SaveEvent>()
    .flatMap(
      (action) => Stream.fromFuture(remoteDb.saveEvent(store.state.authState.userId, action.event))
      .map((_) => saveEventSuccess())
      .cast<EventAction>()
      .onErrorResume(
        (error) => Stream.fromIterable([saveEventError(error.toString())])
      )
      .doOnData((action) {
        if (action is SaveEventSuccess) {
          return nav.pop();
        }
      })
    );
  };
}

Epic<AppState> getEventEpic(RemoteDb remoteDb) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<GetEvent>()
    .flatMap(
      (action) => Stream.fromFuture(remoteDb.getEvent(action.userId, action.eventId))
      .map((event) => getEventSuccess(event))
      .cast<EventAction>()
      .onErrorResume(
        (error) => Stream.fromIterable([getEventError(error.toString())])
      )
    );
  };
}

Epic<AppState> deleteEventEpic(RemoteDb remoteDb, Nav nav) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<DeleteEvent>()
    .flatMap(
      (action) => Stream.fromFuture(remoteDb.deleteEvent(action.userId, action.eventId))
      .map((_) => deleteEventSuccess())
      .cast<EventAction>()
      .onErrorResume(
        (error) => Stream.fromIterable([deleteEventError(error.toString())])
      )
      .doOnData((action) {
        if (action is DeleteEventSuccess) {
          return nav.pop();
        }
      })
    );
  };
}
