import 'package:equatable/equatable.dart';

import '../reduxToolkit.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';

class EventAction extends Action {}

class EventState extends Equatable {
  @override
  List<Object> get props => [loading, error, event, ignoreChange];
  @override
  bool get stringify => true;

  final bool loading;
  final String error;
  final Event event;
  final bool ignoreChange;

  EventState({
    this.loading,
    this.error,
    this.event,
    this.ignoreChange = false,
  });

  EventState copyWith({
    bool loading,
    String error,
    Event event,
    bool ignoreChange,
  }) {
    return EventState(
      loading: loading ?? this.loading,
      error: error ?? this.error,
      event: event ?? this.event,
      ignoreChange: ignoreChange ?? false,
    );
  }
}

class NewEvent extends EventAction {}

class NameChanged extends EventAction {
  final String name;
  NameChanged(this.name);
}

class SaveEvent extends EventAction {
  final Event event;
  SaveEvent(this.event);
}

class SaveEventSuccess extends EventAction with EquatableMixin {
  @override
  List<Object> get props => [];
  @override
  bool get stringify => true;
}

class SaveEventError extends EventAction with EquatableMixin {
  @override
  List<Object> get props => [error];
  @override
  bool get stringify => true;

  final String error;
  SaveEventError(this.error);
}

class GetEvent extends EventAction {
  final String userId;
  final String eventId;
  GetEvent(this.userId, this.eventId);
}

class GetEventSuccess extends EventAction with EquatableMixin {
  @override
  List<Object> get props => [event];
  @override
  bool get stringify => true;

  final Event event;
  GetEventSuccess(this.event);
}

class GetEventError extends EventAction with EquatableMixin {
  @override
  List<Object> get props => [error];
  @override
  bool get stringify => true;

  final String error;
  GetEventError(this.error);
}

class DeleteEvent extends EventAction {
  final String userId;
  final String eventId;
  DeleteEvent(this.userId, this.eventId);
}

class DeleteEventSuccess extends EventAction with EquatableMixin {
  @override
  List<Object> get props => [];
  @override
  bool get stringify => true;
}

class DeleteEventError extends EventAction with EquatableMixin {
  @override
  List<Object> get props => [error];
  @override
  bool get stringify => true;

  final String error;
  DeleteEventError(this.error);
}

class CleanUp extends EventAction {}

NewEvent newEvent() => new NewEvent();
NameChanged nameChanged(String name) => new NameChanged(name);
SaveEvent saveEvent(Event event) => new SaveEvent(event);
SaveEventSuccess saveEventSuccess() => new SaveEventSuccess();
SaveEventError saveEventError(String error) => new SaveEventError(error);
GetEvent getEvent(String userId, String eventId) => new GetEvent(userId, eventId);
GetEventSuccess getEventSuccess(Event event) => new GetEventSuccess(event);
GetEventError getEventError(String error) => new GetEventError(error);
DeleteEvent deleteEvent(String userId, String eventId) => new DeleteEvent(userId, eventId);
DeleteEventSuccess deleteEventSuccess() => new DeleteEventSuccess();
DeleteEventError deleteEventError(String error) => new DeleteEventError(error);
CleanUp cleanUp() => new CleanUp();
