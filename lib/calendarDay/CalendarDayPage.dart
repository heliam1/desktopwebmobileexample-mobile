import 'package:desktopwebmobileexample_mobile/auth/PrivateComponent.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';
import 'package:desktopwebmobileexample_mobile/nav/navActions.dart';
import 'package:desktopwebmobileexample_mobile/ui/FadeOut.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../nav/NavigationBar.dart';
import '../AppState.dart';
import 'calendarDayActions.dart';

class _ViewModel {
  bool loading;
  List<Event> events;
  String error;
  Function(String) onEventPressed;
  VoidCallback onFabPressed;

  _ViewModel(
    this.loading,
    this.events,
    this.error,
    this.onEventPressed,
    this.onFabPressed,
  );
}

class CalendarDayPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {

    return StoreConnector<AppState, _ViewModel>(
      distinct: true,
      onInit: (store) {
        store.dispatch(listenEvents(store.state.authState.userId));
      },
      converter: (store) {
        return _ViewModel(
          store.state.calendarDayState.loading,
          store.state.calendarDayState.events,
          store.state.calendarDayState.error,
          (eventRoute) {
            store.dispatch(push(eventRoute));
          },
          () {
            store.dispatch(push('event/newEvent'));
          },
        );
      },
      builder: (context, vm) {
        print('${DateTime.now()} Building: loaded ${vm.loading} error ${vm.error}');
        List<Widget> children = [];

        children.add(calendarDayView(context, vm));
        if (vm.loading) { children.add(loadingView()); }
        if (vm.error != '') { children.add(errorView(vm.error)); print('${DateTime.now()} error added'); }

        return Scaffold(
          body: SafeArea(
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    child: SingleChildScrollView(
                      child: Container(
                        height: MediaQuery.of(context).size.height - 50 - MediaQuery.of(context).padding.top,
                        width: MediaQuery.of(context).size.width,
                        child: Stack(children: children),
                      ),
                    ),
                  ),
                ),
                NavigationBar(
                  2,
                ),
              ]
            ),
          ),
          floatingActionButton: Padding(
            padding: EdgeInsets.only(bottom: 60, right: 15),
            child: FloatingActionButton(
              onPressed: vm.onFabPressed,
              tooltip: 'GoTo',
              child: Icon(Icons.add),
            ),
          ),
        );
      },
    );
  }

  Widget calendarDayView(BuildContext context, _ViewModel vm) {
    return PrivateComponent(
      child: ListView.builder(
        itemCount: vm.events.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () => vm.onEventPressed('event/${vm.events[index].id}'),
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    vm.events[index].id,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20
                    ),
                  ),
                  Text(vm.events[index].name),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget loadingView() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget errorView(String error) {
    return Center(
      child: FadeOut(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          child: Text(error),
        ),
      ),
    );
  }
}
