import 'package:desktopwebmobileexample_mobile/remoteDb/RemoteDb.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../AppState.dart';
import 'calendarDayActions.dart';

Epic<AppState> calendarDayEpic(RemoteDb remoteDb) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<ListenEvents>()
    .flatMap(
      (action) => remoteDb.listenEvents(action.userId)
      .map((events) => receiveEvents(events))
      .cast<CalendarDayAction>()
      .onErrorResume(
        (error) => Stream.fromIterable([calendarDayError(error.toString())])
      )
    );
  };
}
