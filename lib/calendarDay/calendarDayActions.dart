import 'package:equatable/equatable.dart';

import '../reduxToolkit.dart';
import 'package:desktopwebmobileexample_mobile/models/Event.dart';

class CalendarDayAction extends Action {}

class CalendarDayState extends Equatable {
  @override
  List<Object> get props => [loading, error, events];
  @override
  bool get stringify => true;

  final bool loading;
  final String error;
  final List<Event> events;

  CalendarDayState({
    this.loading,
    this.error,
    this.events,
  });

  CalendarDayState copyWith({
    bool loading,
    String error,
    List<Event> events,
  }) {
    return CalendarDayState(
      loading: loading ?? this.loading,
      error: error ?? this.error,
      events: events ?? this.events,
    );
  }
}

class ListenEvents extends CalendarDayAction {
  String userId;
  ListenEvents(
    this.userId
  );
}

class ReceiveEvents extends CalendarDayAction with EquatableMixin {
  @override
  List<Object> get props => [events];
  @override
  bool get stringify => true;

  List<Event> events;
  ReceiveEvents(
    this.events
  );
}

class CalendarDayError extends CalendarDayAction with EquatableMixin {
  @override
  List<Object> get props => [error];
  @override
  bool get stringify => true;

  final String error;
  CalendarDayError(this.error);
}

ListenEvents listenEvents(String userId) => new ListenEvents(userId);
ReceiveEvents receiveEvents(List<Event> events) => new ReceiveEvents(events);
CalendarDayError calendarDayError(String error) => new CalendarDayError(error);
