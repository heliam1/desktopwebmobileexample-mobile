import 'package:desktopwebmobileexample_mobile/reduxToolkit.dart';
import 'package:desktopwebmobileexample_mobile/auth/resetActions.dart';

import 'calendarDayActions.dart';

final CalendarDayState initialCalendarDayState = new CalendarDayState(
  loading: false,
  error: '',
  events: [],
);

CalendarDayState calendarDayReducer(CalendarDayState state, Action action) {
  if (action is ListenEvents) {
    return state.copyWith(
      loading: true,
      error: '',
    );
  } else if (action is ReceiveEvents) {
    return state.copyWith(
      loading: false,
      events: action.events,
      error: '',
    );
  } else if (action is CalendarDayError) {
    return state.copyWith(
      loading: false,
      error: action.error,
    );
  } else if (action is Reset) {
    return initialCalendarDayState;
  } else {
    return state;
  }
}
