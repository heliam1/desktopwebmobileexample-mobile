import 'package:desktopwebmobileexample_mobile/settings/settingsActions.dart';
import 'package:flutter/material.dart';

import 'auth/authActions.dart';
import 'home/homeActions.dart';
import 'counter/counterActions.dart';
import 'calendarDay/calendarDayActions.dart';
import 'event/eventActions.dart';

class AppState {
  final AuthState authState;
  final HomeState homeState;
  final CounterState counterState;
  final CalendarDayState calendarDayState;
  final EventState eventState;
  final SettingsState settingsState;

  AppState({
    @required this.authState,
    @required this.homeState,
    @required this.counterState,
    @required this.calendarDayState,
    @required this.eventState,
    @required this.settingsState,
  });
}
