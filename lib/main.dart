import 'package:desktopwebmobileexample_mobile/calendarDay/calendarDayReducer.dart';
import 'package:desktopwebmobileexample_mobile/clientLogging/ClientLogging.dart';
import 'package:desktopwebmobileexample_mobile/clientLogging/ClientLoggingImpl.dart';
import 'package:desktopwebmobileexample_mobile/clientLogging/clientLoggingDb/ClientLoggingDbFirebase.dart';
import 'package:desktopwebmobileexample_mobile/event/eventReducer.dart';
import 'package:desktopwebmobileexample_mobile/settings/settingsReducer.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:firebase_core/firebase_core.dart';

import 'auth/Auth.dart';
import 'auth/AuthFirebase.dart';
import 'nav/Nav.dart';
import 'nav/NavImpl.dart';
import 'remoteDb/RemoteDb.dart';
import 'remoteDb/RemoteDbFirebase.dart';

import 'AppState.dart';
import 'App.dart';
import 'rootEpic.dart';
import 'rootReducer.dart';
import 'auth/authReducer.dart';
import 'home/homeReducer.dart';
import 'counter/counterReducer.dart';

class Dependencies {
  RemoteDb remoteDb;
  Auth auth;
  Nav nav;
  ClientLogging logging;
  String userAgent;

  Dependencies({
    this.nav,
    this.auth,
    this.remoteDb,
    this.logging,
    this.userAgent,
  });
}

Future<App> createApp() async {
  final FirebaseApp app = await Firebase.initializeApp();

  final Nav nav = NavImpl(GlobalKey<NavigatorState>());
  final AuthFirebase auth = new AuthFirebase(app);

  Dependencies dependencies = new Dependencies(
    nav: nav,
    auth: auth,
    remoteDb: new RemoteDbFirebase(app),
    logging: new ClientLoggingImpl(new ClientLoggingDbFirebase(app)),
    userAgent: 'android',
  );

  final store = Store<AppState>(
    rootReducer,
    middleware: [
      new EpicMiddleware(rootEpic(dependencies)),
    ],
    initialState: AppState(
      authState: initialAuthState,
      homeState: initialHomeState,
      counterState: initialCounterState,
      calendarDayState: initialCalendarDayState,
      eventState: initialEventState,
      settingsState: initialSettingsState,
    ),
    syncStream: false,
    distinct: true,
  );

  auth.activateAuthStateListener(store);

  return App(
    store: store,
    nav: nav,
  );
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(await createApp());
}
