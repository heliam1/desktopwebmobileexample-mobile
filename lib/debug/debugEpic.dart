import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../AppState.dart';

Epic<AppState> debugEpic() {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .doOnData((action) {
      String message = action.toString();
      print('Action: $message');
    })
    .where((action) => false);
  };
}