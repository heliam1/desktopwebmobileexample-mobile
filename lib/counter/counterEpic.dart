import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../AppState.dart';
import '../remoteDb/RemoteDbFirebase.dart';

import 'counterActions.dart';

Epic<AppState> counterEpic(RemoteDbFirebase remoteDb) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<ListenCounter>()
    // .doOnData((event) { print('ListenCounter'); })
    .flatMap((action) => remoteDb.listenCount())
    .map((count) => new ListenCounterSuccess(count));
  };
}

Epic<AppState> incrementEpic(RemoteDbFirebase remoteDb) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<Increment>()
    .flatMap((action) => Stream.fromFuture(remoteDb.increment(action.count)))
    .map((count) => new IncrementSuccess());
  };
}

// Redux Epic Recipes https://pub.dev/packages/redux_epics
