import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../nav/NavigationBar.dart';
import '../AppState.dart';
import 'counterActions.dart';

class CounterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<Widget> children = [];

    children.add(
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            StoreConnector<AppState, String>(
              onInit: (store) {
                store.dispatch(listenCounter());
              },
              // store.state is actually store.AppState
              converter: (store) => store.state.counterState.count.toString(),
              builder: (context, count) {
                return Text(
                  count,
                  style: Theme.of(context).textTheme.headline4,
                );
              },
            ),
          ],
        ),
      ),
    );

    return Scaffold(
      body: Column(children: [
        Expanded(
          child: Container(
            child: SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height - 50 - 50,
                width: MediaQuery.of(context).size.width,
                child: Stack(children: children),
              ),
            ),
          ),
        ),
        NavigationBar(
          1,
        ),
      ]),
      floatingActionButton: StoreConnector<AppState, VoidCallback>(
        converter: (store) {
          return () =>
              store.dispatch(increment(store.state.counterState.count));
        },
        builder: (context, callback) {
          return Padding(
            padding: EdgeInsets.only(bottom: 60, right: 15),
            child: FloatingActionButton(
              onPressed: callback,
              tooltip: 'Increment',
              child: Icon(Icons.add),
            ),
          );
        },
      ),
    );
  }
}
