import '../reduxToolkit.dart';

class CounterAction extends Action {}

class CounterState {
  final bool loading;
  final int count;
  final String error;

  const CounterState({
    this.loading,
    this.count,
    this.error
  });

  CounterState copyWith({
    bool loading,
    int count,
    String error
  }) {
    return CounterState(
      loading: loading ?? this.loading,
      count: count ?? this.count,
      error: error ?? this.error
    );
  }
}

class ListenCounter extends CounterAction {}

class ListenCounterSuccess extends CounterAction {
  int count;
  ListenCounterSuccess(this.count);
}

class Increment extends CounterAction {
  int count;
  Increment(this.count);
}

class IncrementSuccess extends CounterAction {}

class CounterError extends CounterAction {
  String error;
  CounterError(this.error);
}

ListenCounter listenCounter() => new ListenCounter();
ListenCounterSuccess listenCounterSuccess(int count) => new ListenCounterSuccess(count);
Increment increment(int count) => new Increment(count);
IncrementSuccess incrementSuccess() => new IncrementSuccess();
CounterError counterError(String error) => new CounterError(error);
