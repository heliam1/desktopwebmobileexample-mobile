import 'package:desktopwebmobileexample_mobile/auth/resetActions.dart';
import 'package:desktopwebmobileexample_mobile/reduxToolkit.dart';

import 'counterActions.dart';

final CounterState initialCounterState = new CounterState(
  loading: true,
  count: 0,
  error: ''
);

CounterState counterReducer(CounterState state, Action action) {
  if (action is ListenCounter) {
    return state.copyWith(
      loading: true,
      error: '',
    );
  } else if (action is ListenCounterSuccess) {
    return state.copyWith(
      loading: false,
      count: action.count,
      error: '',
    );
  } else if (action is Increment) {
    return state.copyWith(
      loading: true,
      error: '',
    );
  } else if (action is IncrementSuccess) {
    return state.copyWith(
      loading: false,
      error: '',
    );
  } else if (action is CounterError) {
    return state.copyWith(
      loading: false,
      error: action.error,
    );
  } else if (action is Reset) {
    return initialCounterState;
  } else {
    return state;
  }
}
