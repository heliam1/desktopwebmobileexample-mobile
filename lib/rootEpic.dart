import 'main.dart';
import 'AppState.dart';
import 'package:redux_epics/redux_epics.dart';

import 'auth/authEpic.dart';
import 'home/homeEpic.dart';
import 'nav/navEpic.dart';
import 'counter/counterEpic.dart';
import 'calendarDay/calendarDayEpic.dart';
import 'event/eventEpic.dart';
import 'settings/settingsEpic.dart';

import 'remoteDb/remoteDbEpic.dart';
import 'debug/debugEpic.dart';

Epic<AppState> rootEpic(Dependencies dependencies) {
  // TODO: if dev mode no debug epic
  return combineEpics<dynamic>([
    signedInEpic(dependencies.nav),
    homeAnonLogInEpic(dependencies.auth, dependencies.logging, dependencies.userAgent),
    homeLogInEpic(dependencies.auth, dependencies.nav, dependencies.logging, dependencies.userAgent),
    homeSignUpEpic(dependencies.auth),
    popAllAndPushEpic(dependencies.nav),
    popEpic(dependencies.nav),
    pushEpic(dependencies.nav),
    counterEpic(dependencies.remoteDb),
    incrementEpic(dependencies.remoteDb),
    calendarDayEpic(dependencies.remoteDb),
    saveEventEpic(dependencies.remoteDb, dependencies.nav),
    getEventEpic(dependencies.remoteDb),
    deleteEventEpic(dependencies.remoteDb, dependencies.nav),
    logOutEpic(dependencies.nav, dependencies.auth),
    deleteAccountEpic(dependencies.nav, dependencies.auth),
    remoteDbEpic(dependencies.remoteDb),
    debugEpic()
  ]);
}
