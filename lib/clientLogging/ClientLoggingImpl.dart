import 'package:desktopwebmobileexample_mobile/clientLogging/ClientLogging.dart';
import 'package:desktopwebmobileexample_mobile/clientLogging/clientLoggingDb/ClientLoggingDb.dart';
import 'package:desktopwebmobileexample_mobile/models/Log.dart';

class _Trace {
  String userId;
  bool anon;
  DateTime startTime;
  String userAgent; 
  int attempts;

  _Trace(
    this.userId,
    this.anon,
    this.startTime,
    this.userAgent,
    this.attempts,
  );
}

class ClientLoggingImpl implements ClientLogging {

  final ClientLoggingDb db;
  final Map<String, _Trace> activeTraces = {};

  ClientLoggingImpl(this.db);
  
  void startTrace(
    String nameOfTrace,
    DateTime startTime,
    String userId,
    bool anon,
    String userAgent,
    int timeoutDurationMs,
  ) {
    try {
      _Trace trace = activeTraces[nameOfTrace];

      if (trace != null) {
        this.activeTraces[nameOfTrace].attempts++;
      } else {
        this.activeTraces[nameOfTrace] = _Trace(
          userId, anon, startTime, userAgent, 1
        );

        Future.delayed(Duration(milliseconds: timeoutDurationMs))
        .then((value) {
          _Trace timedOutTrace = this.activeTraces[nameOfTrace];
          if (timedOutTrace != null) {
            this.activeTraces.remove(nameOfTrace);
            Log log = new Log(
              nameOfTrace, startTime, startTime.toIso8601String(), -1, userId,
              anon, userAgent, true, timedOutTrace.attempts, ''
            );
            this.db.writeLog(log);
          }
        });
      }
    } catch (err) {
      print('startTrace error: $err');
    }
    
  }
  
  bool endTrace(
    String nameOfTrace,
    String userId,
    bool anon,
  ) {
    try {
      _Trace trace = this.activeTraces[nameOfTrace];

      if (trace != null) {
        this.activeTraces.remove(nameOfTrace);
        int latency = new DateTime.now().millisecondsSinceEpoch - trace.startTime.millisecondsSinceEpoch;
        Log log = new Log(
          nameOfTrace,
          trace.startTime,
          trace.startTime.toIso8601String(),
          latency,
          userId,
          anon,
          trace.userAgent,
          true,
          trace.attempts,
          '',
        );
        this.db.writeLog(log);
        return true;
      }
      return false;
    } catch (err) {
      print('endTrace error: $err');
      return false;
    }
    
  }
  
  bool endTraceWithError(
    String nameOfTrace,
    String userId,
    bool anon,
    String error,
  ) {
    try {
      _Trace trace = this.activeTraces[nameOfTrace];

      if (trace != null) {
        this.activeTraces.remove(nameOfTrace);
        int latency = new DateTime.now().millisecondsSinceEpoch - trace.startTime.millisecondsSinceEpoch;
        Log log = new Log(
          nameOfTrace,
          trace.startTime,
          trace.startTime.toIso8601String(),
          latency,
          userId,
          anon,
          trace.userAgent,
          true,
          trace.attempts,
          error,
        );
        this.db.writeLog(log);
        return true;
      }
      return false;
    } catch (err) { return false; }
  }
}
