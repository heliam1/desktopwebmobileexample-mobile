import 'package:desktopwebmobileexample_mobile/models/Log.dart';

abstract class ClientLoggingDb {
  void writeLog(Log log);
}
