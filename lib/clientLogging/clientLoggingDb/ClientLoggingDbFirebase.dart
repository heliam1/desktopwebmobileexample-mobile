import 'package:desktopwebmobileexample_mobile/clientLogging/clientLoggingDb/ClientLoggingDb.dart';
import 'package:desktopwebmobileexample_mobile/models/Log.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

class ClientLoggingDbFirebase implements ClientLoggingDb {
  FirebaseDatabase _rtdb;

  ClientLoggingDbFirebase(FirebaseApp app) {
    _rtdb = FirebaseDatabase.instance;
  }

  void writeLog(Log log) {
    this._rtdb
    .reference()
    .child('logs/${log.start.millisecondsSinceEpoch}_${log.name}_${log.userId}')
    .set({
      'name': log.name,
      'start': log.start.millisecondsSinceEpoch,
      'startIso': log.startIso,
      'latency': log.latency,
      'userId': log.userId,
      'anon': log.anon,
      'userAgent': log.userAgent,
      'unread': log.unread,
      'attempts': log.attempts,
      'error': log.error,
    })
    .catchError((err) {
      print('Failed to log: $err');
      print('logs/${log.start.millisecondsSinceEpoch}_${log.name}_${log.userId}');
      print(log.start.millisecondsSinceEpoch);
      print(log);
    });
  }
}
