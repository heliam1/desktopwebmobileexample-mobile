abstract class ClientLogging {
  void startTrace(
    String nameOfTrace,
    DateTime startTime,
    String userId,
    bool anon,
    String userAgent,
    int timeoutDurationMs,
  );
  
  bool endTrace(
    String nameOfTrace,
    String userId,
    bool anon,
  );
  
  bool endTraceWithError(
    String nameOfTrace,
    String userId,
    bool anon,
    String error,
  );
}
