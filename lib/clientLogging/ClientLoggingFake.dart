import 'package:desktopwebmobileexample_mobile/clientLogging/ClientLogging.dart';

class ClientLoggingFake implements ClientLogging {
  
  void startTrace(
    String nameOfTrace,
    DateTime startTime,
    String userId,
    bool anon,
    String userAgent,
    int timeoutDurationMs,
  ) {

  }
  
  bool endTrace(
    String nameOfTrace,
    String userId,
    bool anon,
  ) {
    return true;
  }
  
  bool endTraceWithError(
    String nameOfTrace,
    String userId,
    bool anon,
    String error,
  ) {
    return true;
  }
}
