import 'package:desktopwebmobileexample_mobile/ui/FadeOut.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../AppState.dart';
import 'homeActions.dart';

class _ViewModel {
  bool loading;
  String error;
  VoidCallback onLogInPressed;
  VoidCallback onSignUpPressed;

  _ViewModel(
    this.loading,
    this.error,
    this.onLogInPressed,
    this.onSignUpPressed,
  );
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _email;
  String _password;
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
    void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
    } 

  @override
  Widget build(BuildContext context) {

    return StoreConnector<AppState, _ViewModel>(
      distinct: true,
      onInit: (store) {
        if (!store.state.authState.authenticated) {
          store.dispatch(anonLogIn());
        }
      },
      converter: (store) {
        return _ViewModel(
          store.state.homeState.loading,
          store.state.homeState.error,
          () { store.dispatch(logIn(_email, _password)); },
          () { store.dispatch(signUp(_email, _password)); },
        );
      },
      builder: (context, vm) {
        List<Widget> children = [];

        children.add(homeView(context, vm));
        if (vm.loading) { children.add(loadingView()); }
        if (vm.error != '') { children.add(errorView(vm.error)); }

        return Scaffold(
          body: SingleChildScrollView(
            child: SafeArea(
              child: SizedBox(
                height: MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top,
                width: MediaQuery.of(context).size.width,
                child: Stack(children: children),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget homeView(BuildContext context, _ViewModel vm) {
    return Card(
      margin: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 10),
      elevation: 5,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Padding(
            padding: EdgeInsets.only(left: 120, right: 120, top: 0),
            child: Image(
              image: AssetImage('assets/chronos-logo-smaller-numerals.png'),
            ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 30, bottom: 30),
            child: Text(
              "CHRONOS",
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            width: 250,
            child: TextField(
              key: ValueKey('home_email'),
              cursorColor: const Color(0xff90caf9),
              controller: emailController,
              onChanged: (email) {
                setState(() {
                  _email = email;
                });
              },
              decoration: InputDecoration(
                hintText: "Email",
                focusedBorder: new UnderlineInputBorder(
                  borderSide: new BorderSide(
                    color: const Color(0xff90caf9),
                    width: 3,
                  ),
                ),
              ),
            ),
          ),
          Container(
            width: 250,
            child: TextField(
              key: ValueKey('home_password'),
              cursorColor: const Color(0xff90caf9),
              obscureText: true,
              controller: passwordController,
              onChanged: (password) {
                setState(() {
                  _password = password;
                });
              },
              // ignore: todo
              // TODO: onSubmitted: viewModel.loginViaSubmitted,
              decoration: InputDecoration(
                hintText: "Password",
                focusedBorder: new UnderlineInputBorder(
                  borderSide: new BorderSide(
                    color: const Color(0xff90caf9),
                    width: 3,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: Container(
              width: 150,
              decoration: BoxDecoration(
                color: const Color(0xff90caf9),
                border: Border.all(color: const Color(0xff90caf9)),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: MaterialButton(
                // color: Color(0xcefdff),
                child: Text("Login"),
                onPressed: vm.onLogInPressed,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: Container(
              width: 150,
              decoration: BoxDecoration(
                // color: Colors.blue,
                border: Border.all(color: const Color(0xff90caf9)),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: MaterialButton(
                // color: Color(0xcefdff),
                child: Text("Sign Up"),
                onPressed: vm.onSignUpPressed,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: GestureDetector(
              child: Text(
                "Forgot password",
                style: TextStyle(color: const Color(0xff90caf9)),
              ),
              onTap: () {
                // ignore: todo
                // TODO: password reset
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget loadingView() {
    return Center(
      child: CircularProgressIndicator()
    );
  }

  Widget errorView(String error) {
    return Center(
      child: FadeOut(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          child: Text(error),
        ),
      ),
    );
  }
}


