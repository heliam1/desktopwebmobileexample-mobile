import 'package:desktopwebmobileexample_mobile/auth/resetActions.dart';
import 'package:desktopwebmobileexample_mobile/reduxToolkit.dart';

import 'homeActions.dart';

final HomeState initialHomeState = new HomeState(
  loading: false,
  error: '',
  success: false,
);

HomeState homeReducer(HomeState state, Action action) {
  if (action is AnonLogIn) {
    return state.copyWith();
  } else if (action is AnonLogInSuccess) {
    return state.copyWith();
  } else if (action is LogIn) {
    return state.copyWith(
      loading: true,
      error: '',
      success: false,
    );
  } else if (action is LogInSuccess) {
    return state.copyWith(
      loading: false,
      error: '',
      success: false,
    );
  } else if (action is SignUp) {
    return state.copyWith(
      loading: true,
      error: '',
      success: false,
    );
  } else if (action is SignUpSuccess) {
    return state.copyWith(
      loading: false,
        error: '',
        success: true,
    );
  } else if (action is HomeError) {
    return state.copyWith(
      loading: false,
      error: action.error,
      success: false,
    );
  } else if (action is Reset) {
    return initialHomeState;
  } else {
    return state;
  }
}
