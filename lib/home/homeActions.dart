import 'package:equatable/equatable.dart';

import '../reduxToolkit.dart';

class HomeAction extends Action {}

class HomeState extends Equatable {
  @override
  List<Object> get props => [loading, error, success];
  @override
  bool get stringify => true;
  
  final bool loading;
  final String error;
  final bool success;

  HomeState({
    this.loading,
    this.error,
    this.success,
  });

  HomeState copyWith({
    bool loading,
    String error,
    bool success,
  }) {
    return HomeState(
      loading: loading ?? this.loading,
      error: error ?? this.error,
      success: success ?? this.success,
    );
  }
}

class AnonLogIn extends HomeAction {}

class AnonLogInSuccess extends HomeAction with EquatableMixin {
  @override
  List<Object> get props => [];
  @override
  bool get stringify => true;
}

class LogIn extends HomeAction { 
  final String email;
  final String password;
  LogIn(
    this.email,
    this.password,
  );
}

class LogInSuccess extends HomeAction with EquatableMixin {
  @override
  List<Object> get props => [];
  @override
  bool get stringify => true;
}

class SignUp extends HomeAction {
  final String email;
  final String password;
  SignUp(
    this.email,
    this.password,
  );
}

class SignUpSuccess extends HomeAction with EquatableMixin {
  @override
  List<Object> get props => [];
  @override
  bool get stringify => true;
}

class HomeError extends HomeAction with EquatableMixin {
  @override
  List<Object> get props => [error];
  @override
  bool get stringify => true;

  final String error;
  HomeError(this.error);
}

AnonLogIn anonLogIn() => new AnonLogIn();
AnonLogInSuccess anonLogInSuccess() => new AnonLogInSuccess();
LogIn logIn(String email, String password) => new LogIn(email, password);
LogInSuccess logInSuccess() => new LogInSuccess();
SignUp signUp(String email, String password) => new SignUp(email, password);
SignUpSuccess signUpSuccess() => new SignUpSuccess();
HomeError homeError(String error) => new HomeError(error);
