import 'package:desktopwebmobileexample_mobile/auth/Auth.dart';
import 'package:desktopwebmobileexample_mobile/clientLogging/ClientLogging.dart';
import 'package:desktopwebmobileexample_mobile/constants/routes.dart';
import 'package:desktopwebmobileexample_mobile/nav/Nav.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../AppState.dart';
import 'homeActions.dart';

Epic<AppState> homeAnonLogInEpic(Auth auth, ClientLogging logging, String userAgent) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<AnonLogIn>()
    .doOnData((_) => logging.startTrace('ANON_LOG_IN_AND_DOWNLOAD_URL', new DateTime.now(), auth.userId(), auth.isAnon(), userAgent, 60000))
    .flatMap((action) {
      return Stream.fromFuture(auth.logInAnonymously())
      .map((_) => anonLogInSuccess())
      .doOnData((action) => logging.endTrace('ANON_LOG_IN_AND_DOWNLOAD_URL', auth.userId(), auth.isAnon()))
      .cast<HomeAction>()
      .onErrorResume((error) => Stream.fromIterable([homeError(error.toString())]));
    });
  };
}

Epic<AppState> homeLogInEpic(Auth auth, Nav nav, ClientLogging logging, String userAgent) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<LogIn>()
    .doOnData((action) => logging.startTrace('LOG_IN', new DateTime.now(), auth.userId(), auth.isAnon(), userAgent, 60000))
    .flatMap((action) {
      return Stream.fromFuture(
        auth.logInEmailAndPassword(action.email, action.password)
      )
      .map((_) => LogInSuccess())
      .doOnData((action) => logging.endTrace('LOG_IN', auth.userId(), auth.isAnon()))
      .doOnData((action) {
        nav.pushAndPopAll(ROUTE_CALENDAR_DAY);
      })
      .cast<HomeAction>()
      .onErrorResume((error) => Stream.fromIterable([homeError(error.toString())]));
    });
  };
}

Epic<AppState> homeSignUpEpic(Auth auth) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<SignUp>()
    .flatMap((action) {
      return Stream.fromFuture(
        auth.signUpEmailAndPassword(action.email, action.password)
      ).map((_) => SignUpSuccess())
      .cast<HomeAction>()
      .onErrorResume((error) => Stream.fromIterable([homeError(error.toString())]));
    });
  };
}
