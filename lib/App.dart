import 'package:desktopwebmobileexample_mobile/AppState.dart';
import 'package:desktopwebmobileexample_mobile/constants/routes.dart';
import 'package:desktopwebmobileexample_mobile/routes.dart';
import 'package:desktopwebmobileexample_mobile/nav/NavImpl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';


class App extends StatelessWidget {
  final Store<AppState> store;
  final NavImpl nav;

  App({
    this.store,
    this.nav,
  });

  @override
  Widget build(BuildContext context) {
    return StoreProvider(
      store: store,
      child: MaterialApp(
        navigatorKey: nav.nav,
        title: 'Desktop Web Mobile',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        onGenerateRoute: routes,
        initialRoute: store.state.authState.authenticated
                      ? ROUTE_CALENDAR_DAY : ROUTE_HOME,
      ),
    );
  }
}
