import 'dart:async';
import 'package:redux/redux.dart';
import '../AppState.dart';
import 'Auth.dart';

class AuthFake implements Auth {
  Future<void> logInAnonymously() async {
    return;
  }

  Future<void> deleteAnonymousAccount() async {

  }

  Future<void> signUpEmailAndPassword(String email, String password) async {

  }

  Future<void> logInEmailAndPassword(String email, String password) async {

  }

  Future<void> logOutAnonymousAccount() async {

  }

  Future<void> logOut() async {

  }

  Future<void> sendPasswordResetEmail(String email) async {

  }

  String userId() {
    return 'userId';
  }

  String userEmail() {
    return 'anon';
  }

  bool isAnon() {
    return true;
  }

  Future<void> updateUserEmail(String email) async {}

  Future<void> deleteAccount() async {}

  void activateAuthStateListener(Store<AppState> store) {

  }

  Future<void> dispose() async {

  }
}
