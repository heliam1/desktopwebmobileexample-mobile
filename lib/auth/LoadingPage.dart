import 'package:flutter/material.dart';

class LoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image(
          height: 400,
          width: 400,
          image: AssetImage('assets/chronos-logo-smaller-numerals.png'),
        ),
      ),
    );
  }
}