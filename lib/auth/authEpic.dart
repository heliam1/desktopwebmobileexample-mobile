import 'package:desktopwebmobileexample_mobile/constants/routes.dart';
import 'package:desktopwebmobileexample_mobile/nav/Nav.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../AppState.dart';
import 'authActions.dart';

Epic<AppState> signedInEpic(Nav nav) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<SignedIn>()
    // SignedIn is only for non anon
    .doOnData((action) { 
      nav.pushAndPopAll(ROUTE_CALENDAR_DAY);
    })
    .mapTo(signedInSuccess());
  };
}
