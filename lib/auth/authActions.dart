import '../reduxToolkit.dart';

class AuthAction extends Action {}

class AuthState {
  final bool authenticated;
  final bool initialized;
  final String userId;

  AuthState({
    this.authenticated,
    this.initialized,
    this.userId,
  });

  AuthState copyWith({
    bool authenticated,
    bool initialized,
    String userId
  }) {
    return AuthState(
      authenticated: authenticated ?? this.authenticated,
      initialized: initialized ?? this.initialized,
      userId: userId ?? this.userId,
    );
  }
}

class SignedIn extends AuthAction {
  final String userId;
  SignedIn(this.userId);
}

class SignedInSuccess extends AuthAction {}

class SignedOut extends AuthAction {}

class AppLoaded extends AuthAction {}

SignedIn signedIn(String userId) => new SignedIn(userId);
SignedInSuccess signedInSuccess() => new SignedInSuccess();
SignedOut signedOut() => new SignedOut();
AppLoaded appLoaded() => new AppLoaded();
