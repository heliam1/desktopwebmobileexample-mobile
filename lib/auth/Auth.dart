import 'dart:async';
import 'package:redux/redux.dart';
import '../AppState.dart';

abstract class Auth {
  Future<void> logInAnonymously();
  Future<void> deleteAnonymousAccount();
  Future<void> signUpEmailAndPassword(String email, String password);
  Future<void> logInEmailAndPassword(String email, String password);
  Future<void> logOutAnonymousAccount();
  Future<void> logOut();
  Future<void> sendPasswordResetEmail(String email);
  String userEmail();
  String userId();
  bool isAnon();
  Future<void> updateUserEmail(String email);
  Future<void> deleteAccount();
  void activateAuthStateListener(Store<AppState> store);
  Future<void> dispose();
}
