import 'dart:async';

import 'package:desktopwebmobileexample_mobile/auth/Auth.dart';
import 'package:desktopwebmobileexample_mobile/home/homeActions.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_functions/cloud_functions.dart';

import 'package:redux/redux.dart';

import '../AppState.dart';
import 'authActions.dart';
import 'resetActions.dart';
import '../remoteDb/remoteDbActions.dart';

class AuthFirebase implements Auth {
  FirebaseAuth _auth;
  FirebaseFunctions _functions;

  StreamSubscription<User> _authStateStream;

  AuthFirebase(FirebaseApp app) {
    _auth = FirebaseAuth.instanceFor(app: app);
    _functions = FirebaseFunctions.instanceFor(app: app);
  }

  Future<void> logInAnonymously() async {
    User user = this._auth.currentUser;
    if (user == null || (!user.isAnonymous && user.email != null)) {
      print('Signing in anonymously');
      await this._auth.signInAnonymously();
    }
  }

  bool isAnon() {
    User user = this._auth.currentUser;
    if (user != null) {
      return user.isAnonymous;
    } else {
      return true;
    }
  }

  Future<void> deleteAnonymousAccount() async {
    User user = this._auth.currentUser;
    if (user != null && user.isAnonymous) {
      return user.delete();
    }
  }

  Future<void> signUpEmailAndPassword(String email, String password) {
    final HttpsCallable callable = _functions.httpsCallable(
      'signUpUserWithEmailAndPassword',
    );
    return callable
    .call(<String, dynamic>{'email': email, 'password': password});
  }

  Future<void> logInEmailAndPassword(String email, String password) async {
    try {
      await this.deleteAnonymousAccount();
      await this.logOutAnonymousAccount();
      await this._auth
      .signInWithEmailAndPassword(email: email, password: password);
    } catch (error) {
      await this.logInAnonymously();
      throw error;
    }
    return _auth.signInWithEmailAndPassword(email: email, password: password);
  }

  Future<void> logOutAnonymousAccount() async {
    if (
      this._auth.currentUser != null
      && this._auth.currentUser.isAnonymous
    ) {
      await this._auth.signOut();
    }
  }

  Future<void> logOut() async {
    if (
      this._auth.currentUser != null
      && !this._auth.currentUser.isAnonymous
    ) {
      await this._auth.signOut();
    }
  }

  Future<void> sendPasswordResetEmail(String email) {
    return this._auth.sendPasswordResetEmail(email: email);
  }

  String userEmail() {
    User user = this._auth.currentUser;
    if (user != null) {
      return user.email;
    } else {
      return 'anon';
    }
  }

  String userId() {
    User user = this._auth.currentUser;
    if (user != null) {
      print('return user.uid;');
      return user.uid;
    } else {
      print('return anon');
      return 'anon';
    }
  }

  Future<void> updateUserEmail(String email) {
    User user = this._auth.currentUser;
    return user.updateEmail(email);
  }

  Future<void> deleteAccount() async {
    final HttpsCallable callable = _functions.httpsCallable(
      'deleteAccount',
    );

    User user = this._auth.currentUser;

    if (user != null && !user.isAnonymous) {
      return callable.call(<String, dynamic>{
        'userId': user.uid,
      }).then((_) {
        return logOut();
      });
    }
  }

  // TODO: consider auth epic that is utilized by root component
  void activateAuthStateListener(Store<AppState> store) {
    if (_authStateStream == null) {
      _authStateStream = this._auth
      .authStateChanges() 
      .listen((user) {
        if (user != null && !user.isAnonymous && user.email != null) {
          print('User signed in. uid=${user.uid}. anon=${user.isAnonymous}. email=${user.email}');
          store.dispatch(signedIn(user.uid));
          store.dispatch(appLoaded());
        } else {
          print('User signed out.');
          // TODO: consider having an epic received SignedOut to force nav or display modal
          store.dispatch(signedOut());
          store.dispatch(disposeRemoteDb());
          store.dispatch(reset());
          store.dispatch(appLoaded());
        }
      });
    } else {
      print('activateAuthStateListener() should not be called twice.');
    }
  }

  Future<void> dispose() async {
    print("auth: disposal");
    await _authStateStream.cancel();
  }
}
