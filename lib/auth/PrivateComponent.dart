import 'package:desktopwebmobileexample_mobile/nav/navActions.dart';
import 'package:desktopwebmobileexample_mobile/constants/routes.dart';
import 'package:desktopwebmobileexample_mobile/util/Pair.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../AppState.dart';

class PrivateComponent extends StatelessWidget {
  final Widget child;

  PrivateComponent({@required this.child});

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, Pair<bool, VoidCallback>>(
      converter: (store) {
        return Pair(
          store.state.authState.authenticated,
          () => store.dispatch(popAllAndPush(ROUTE_HOME))
        );
      },
      builder: (context, authenticated) {
        return child;
      },
      onWillChange: (prev, current) {
        print('PrivateComponent: prev:${prev.first} current:${current.first}');
        if (!current.first && prev.first != current.first && prev.first != null) {
          current.second();
        }
      },
    );
  }
}
