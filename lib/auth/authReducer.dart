import 'package:desktopwebmobileexample_mobile/reduxToolkit.dart';

import 'authActions.dart';

final AuthState initialAuthState = new AuthState(
  authenticated: false,
  initialized: false,
  userId: null,
);

AuthState authReducer(AuthState state, Action action) {
  if (action is SignedIn) {
    return state.copyWith(
      authenticated: true,
      userId: action.userId,
    );
  } else if (action is SignedInSuccess) {
    return state.copyWith();
  } else if (action is SignedOut) {
    return state.copyWith(
      authenticated: false,
      userId: null,
    );
  } else if (action is AppLoaded) {
    return state.copyWith(
      initialized: true,
    );
  } else {
    return state;
  }
}
