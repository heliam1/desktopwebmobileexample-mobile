import 'package:equatable/equatable.dart';

import '../reduxToolkit.dart';

class SettingsAction extends Action {}

class SettingsState extends Equatable {
  @override
  List<Object> get props => [loading, error];
  @override
  bool get stringify => true;

  final bool loading;
  final String error;

  SettingsState({
    this.loading,
    this.error,
  });

  SettingsState copyWith({
    bool loading,
    String error,
  }) {
    return SettingsState(
      loading: loading ?? this.loading,
      error: error ?? this.error,
    );
  }
}

class LogOut extends SettingsAction {}

class DeleteAccount extends SettingsAction {}

class SettingsSuccess extends SettingsAction with EquatableMixin {
  @override
  List<Object> get props => [];
  @override
  bool get stringify => true;
}

class SettingsError extends SettingsAction with EquatableMixin {
  @override
  List<Object> get props => [error];
  @override
  bool get stringify => true;

  final String error;
  SettingsError(this.error);
}

class CleanUp extends SettingsAction {}

LogOut logOut() => new LogOut();
DeleteAccount deleteAccount() => new DeleteAccount();
SettingsSuccess settingsSuccess() => new SettingsSuccess();
SettingsError settingsError(String error) => new SettingsError(error);
CleanUp cleanUp() => new CleanUp();
