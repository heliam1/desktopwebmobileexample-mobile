import 'package:desktopwebmobileexample_mobile/AppState.dart';
import 'package:desktopwebmobileexample_mobile/nav/NavigationBar.dart';
import 'package:desktopwebmobileexample_mobile/ui/FadeOut.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'settingsActions.dart';

class _ViewModel {
  bool loading;
  String error;
  VoidCallback onLogOutPressed;
  VoidCallback onDeleteAccountPressed;

  _ViewModel(
    this.loading,
    this.error,
    this.onLogOutPressed,
    this.onDeleteAccountPressed,
  );
}

class SettingsPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, _ViewModel>(
      distinct: true,
      onDispose: (store) {
        store.dispatch(cleanUp());
      },
      converter: (store) {
        return _ViewModel(
          store.state.settingsState.loading,
          store.state.settingsState.error,
          () { store.dispatch(logOut()); },
          () { store.dispatch(deleteAccount()); },
        );
      },
      builder: (context, vm) {
        List<Widget> children = [];

        children.add(settingsView(context, vm));
        if (vm.loading) { children.add(loadingView()); }
        if (vm.error != '') { children.add(errorView(vm.error)); }

        return Scaffold(
          body: SafeArea(
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    child: SingleChildScrollView(
                      child: Container(
                        height: MediaQuery.of(context).size.height - 50 - MediaQuery.of(context).padding.top,
                        width: MediaQuery.of(context).size.width,
                        child: Stack(
                          children: children
                        ),
                      ),
                    ),
                  ),
                ),
                NavigationBar(
                  5,
                ),
              ],
            ),
          ),
        );
      }
    );
  }

  Widget settingsView(BuildContext context, _ViewModel vm) {
    return Padding(
      padding: EdgeInsets.only(top: 15, left: 15, right: 15, bottom: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          /*Container(
            width: 100,
            height: 50,
          ),
          Center(
            child: Container(
              height: 150,
              width: 150,
              child: Icon(
                Icons.person,
                size: 100,
              ),
              decoration: BoxDecoration(
                color: const Color(0xff90caf9),
                shape: BoxShape.circle,
              ),
            ),
          ),
          Container(
            width: 100,
            height: 50,
          ),
          Container(
            width: 100,
            child: Text("Change email"),
          ),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  width: 215,
                  child: TextField(
                    controller: TextEditingController.fromValue(
                      TextEditingValue(
                        text: ViewModelBuilder().userEmail,
                        selection: TextSelection.collapsed(
                            offset: vm.userEmail.length
                        ),
                      ),
                    ),
                    autofocus: false,
                    onChanged: vm.onEmailChanged,
                  ),
                ),
                Container(
                  width: 15,
                  height: 10,
                ),
                Container(
                  width: 60,
                  height: 40,
                  decoration: BoxDecoration(
                    border: Border.all(color: const Color(0xff90caf9)),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                  ),
                  child: MaterialButton(
                    child: Icon(
                        Icons.check
                    ),
                    onPressed: () {
                      vm.emailChangePressed();
                    },
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: Center(
              child: Container(
                width: 300,
                decoration: BoxDecoration(
                  // color: Colors.blue,
                  border: Border.all(color: const Color(0xff90caf9)),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: MaterialButton(
                  child: Text("Password reset"),
                  onPressed: () {
                    viewModel.passwordReset();
                  },
                ),
              ),
            ),
          ),*/
          Padding(
            padding: EdgeInsets.only(top: 20),
            child: Center(
              child:Container(
                width: 300,
                decoration: BoxDecoration(
                  // color: Colors.blue,
                  border: Border.all(color: const Color(0xff90caf9)),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: MaterialButton(
                  key: ValueKey('settings_sign_out'),
                  child: Text("Log out"),
                  onPressed: vm.onLogOutPressed
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20, bottom: 20),
            child: Center(
              child: GestureDetector(
                key: ValueKey('settings_delete_account'),
                child: Text(
                  "Deactivate account",
                  style: TextStyle(
                      color: const Color(0xff90caf9)
                  ),
                ),
                onTap: vm.onDeleteAccountPressed
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget loadingView() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget errorView(String error) {
    return Center(
      child: FadeOut(
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          child: Text(error),
        ),
      ),
    );
  }
}
