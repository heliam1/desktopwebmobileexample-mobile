import 'package:desktopwebmobileexample_mobile/auth/Auth.dart';
import 'package:desktopwebmobileexample_mobile/constants/routes.dart';
import 'package:desktopwebmobileexample_mobile/nav/Nav.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../AppState.dart';
import 'settingsActions.dart';

Epic<AppState> logOutEpic(Nav nav, Auth auth) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<LogOut>()
    .flatMap((action) {
      return Stream.fromFuture(auth.logOut())
      .mapTo(settingsSuccess())
      .cast<SettingsAction>()
      .doOnData((action) {
        if (action is SettingsSuccess) {
          nav.pushAndPopAll(ROUTE_HOME);
        }
      })
      .onErrorResume((error) => Stream.fromIterable([settingsError(error.toString())]));
    });
  };
}

Epic<AppState> deleteAccountEpic(Nav nav, Auth auth) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<DeleteAccount>()
    .flatMap((action) {
      return Stream.fromFuture(auth.deleteAccount())
      .mapTo(settingsSuccess())
      .cast<SettingsAction>()
      .doOnData((action) {
        nav.pushAndPopAll(ROUTE_HOME);
      })
      .onErrorResume((error) => Stream.fromIterable([settingsError(error.toString())]));
    });
  };
}
