import 'package:desktopwebmobileexample_mobile/auth/resetActions.dart';
import 'package:desktopwebmobileexample_mobile/reduxToolkit.dart';

import 'settingsActions.dart';

final SettingsState initialSettingsState = new SettingsState(
  loading: false,
  error: '',
);

SettingsState settingsReducer(SettingsState state, Action action) {
  if (action is LogOut) {
    return state.copyWith(
      loading: true,
      error: '',
    );
  } else if (action is DeleteAccount) {
    return state.copyWith(
      loading: true,
      error: '',
    );
  } else if (action is SettingsSuccess) {
    return state.copyWith(
      loading: false,
      error: '',
    );
  } else if (action is SettingsError) {
    return state.copyWith(
      loading: false,
      error: action.error,
    );
  } else if (action is CleanUp) {
    return state.copyWith(
      loading: false,
      error: '',
    );
  } else if (action is Reset) {
    return initialSettingsState;
  } else {
    return state;
  }
}
