class Log {
  final String name;
  final DateTime start;
  final String startIso;
  final int latency;
  final String userId;
  final bool anon;
  final String userAgent;
  final bool unread;
  final int attempts;
  String error;

  Log(
    this.name,
    this.start,
    this.startIso,
    this.latency,
    this.userId,
    this.anon,
    this.userAgent,
    this.unread,
    this.attempts,
    String error,  
  ) {
    if (error.length > 32) {
      this.error = error.substring(0, 32);
    } else {
      this.error = error;
    }
  }
}
