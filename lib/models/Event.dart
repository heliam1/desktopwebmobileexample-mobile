import 'package:equatable/equatable.dart';

class Event extends Equatable {
  @override
  List<Object> get props => [id, name];
  @override
  bool get stringify => true;

  final String id;
  final String name;

  Event({
    this.id,
    this.name
  });

  Event copyWith({
    String id,
    String name,
  }) {
    return Event(
      id: id ?? this.id,
      name: name ?? this.name,
    );
  }
}
