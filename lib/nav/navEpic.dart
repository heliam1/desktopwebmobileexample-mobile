import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

import '../AppState.dart';
import 'Nav.dart';
import 'navActions.dart';

Epic<AppState> popAllAndPushEpic(Nav nav) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<PopAllAndPush>()
    .doOnData((action) {
      return nav.pushAndPopAll(action.route);
    })
    .map((action) => navSuccess());
  };
}

Epic<AppState> popEpic(Nav nav) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<Pop>()
    .doOnData((action) {
      return nav.pop();
    })
    .map((action) => navSuccess());
  };
}

Epic<AppState> pushEpic(Nav nav) {
  return (Stream<dynamic> actions, EpicStore<dynamic> store) {
    return actions
    .whereType<Push>()
    .doOnData((action) {
      return nav.push(action.route);
    })
    .map((action) => navSuccess());
  };
}


