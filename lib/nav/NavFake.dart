import 'Nav.dart';

class NavFake implements Nav {

  NavFake();

  void pop() {}
  void push(String route) {}
  void pushAndPopAll(String route) {}
}
