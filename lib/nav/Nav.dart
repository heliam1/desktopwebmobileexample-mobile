abstract class Nav {
  void pop();
  void push(String route);
  void pushAndPopAll(String route);
}
