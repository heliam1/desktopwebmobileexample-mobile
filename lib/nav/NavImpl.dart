import 'package:flutter/material.dart';

import 'Nav.dart';

class NavImpl implements Nav {
  final GlobalKey<NavigatorState> nav;
  // TODO: this stack may not be correct until pushAndPopAll is called once.
  // I'm not sure if we are adding the initial route to the stack.
  // Solve this by only adding to stack via routes.dart? With separate methods?
  final List<String> stack = [];

  NavImpl(
    this.nav
  );

  void pop() {
    if (nav.currentState.canPop()) {
      nav
      .currentState
      .pop();

      stack.removeLast();
    }
  }

  void push(String route) {
    if (stack.isEmpty || stack.last != route) {
      nav
      .currentState
      .pushNamed(route);

      stack.add(route);
    }
  }

  // TODO: If the user quickly navigates to Settings after logging in, they will 
  // still be navigated back to CALENDAR_DAY. (authEpic + homeEpic).
  void pushAndPopAll(String route) {
    if (stack.isEmpty || stack.last != route) {
      nav
      .currentState
      .pushNamedAndRemoveUntil(
        route,
        (route) => route.isCurrent
      );

      if (stack.isNotEmpty) {
        stack.removeRange(0, stack.length - 1);
      }
      stack.add(route);
    }
  }
}
