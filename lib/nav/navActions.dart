import '../reduxToolkit.dart';

class NavAction extends Action {}

class Push extends NavAction {
  String route;
  Push(this.route);
}

class Pop extends NavAction {}

class PopAllAndPush extends NavAction {
  String route;
  PopAllAndPush(this.route);
}

class NavSuccess extends NavAction {}

Push push(String route) => new Push(route);
Pop pop() => new Pop();
PopAllAndPush popAllAndPush(String route) => new PopAllAndPush(route);
NavSuccess navSuccess() => new NavSuccess();
