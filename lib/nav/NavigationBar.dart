import 'package:desktopwebmobileexample_mobile/nav/navActions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import '../AppState.dart';
import '../constants/routes.dart';

class NavigationBar extends StatelessWidget {

  final int _highlighted;

  NavigationBar(
    this._highlighted // From 1 to 5.
  );

  @override
  Widget build(BuildContext context) {

    List<Widget> iconsToAdd = <Widget>[];

    // 1
    iconsToAdd.add(
      StoreConnector<AppState, VoidCallback>(
        converter: (store) {
          return () => store.dispatch(popAllAndPush(ROUTE_COUNTER));
        },
        builder: (context, callback) {
          return GestureDetector(
            key: ValueKey('nav_home'),
            behavior: HitTestBehavior.opaque,
            onTap: callback,
            child: Container(
              // padding: EdgeInsets.only(top: 12, bottom: 12, right: 12),
              child: Icon(Icons.home),
            ),
          );
        }
      )
    );

    // 2
    iconsToAdd.add(
      StoreConnector<AppState, VoidCallback>(
        converter: (store) {
          return () => store.dispatch(popAllAndPush(ROUTE_CALENDAR_DAY));
        },
        builder: (context, callback) {
          return GestureDetector(
            key: ValueKey('nav_calendar'),
            behavior: HitTestBehavior.opaque,
            onTap: callback,
            child: Container(
              // padding: EdgeInsets.only(top: 12, bottom: 12, right: 12),
              child: Icon(Icons.calendar_today),
            ),
          );
        }
      )
    );

    // 3
    iconsToAdd.add(
      StoreConnector<AppState, VoidCallback>(
        converter: (store) {
          return () => store.dispatch(popAllAndPush(ROUTE_COUNTER));
        },
        builder: (context, callback) {
          return GestureDetector(
            key: ValueKey('nav_add'),
            behavior: HitTestBehavior.opaque,
            onTap: callback,
            child: Container(
              // padding: EdgeInsets.only(top: 12, bottom: 12, right: 12),
              child: Icon(Icons.add_circle_outline),
            ),
          );
        }
      )
    );

    // 4
    iconsToAdd.add(
      StoreConnector<AppState, VoidCallback>(
        converter: (store) {
          return () => store.dispatch(popAllAndPush(ROUTE_COUNTER));
        },
        builder: (context, callback) {
          return GestureDetector(
            key: ValueKey('nav_search'),
            behavior: HitTestBehavior.opaque,
            onTap: callback,
            child: Container(
              // padding: EdgeInsets.only(top: 12, bottom: 12, right: 12),
              child: Icon(Icons.search),
            ),
          );
        }
      )
    );

    // 5
    iconsToAdd.add(
      StoreConnector<AppState, VoidCallback>(
        converter: (store) {
          return () => store.dispatch(popAllAndPush(ROUTE_SETTINGS));
        },
        builder: (context, callback) {
          return GestureDetector(
            key: ValueKey('nav_profile'),
            behavior: HitTestBehavior.opaque,
            onTap: callback,
            child: Container(
              // padding: EdgeInsets.only(top: 12, bottom: 12, right: 12),
              child: Icon(Icons.person),
            ),
          );
        }
      )
    );


    for (int i = 0; i < iconsToAdd.length; i++) {
      if (_highlighted - 1 == i) {
        iconsToAdd[i] = Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            color: const Color(0xff90caf9),
            shape: BoxShape.circle,
          ),
          child: iconsToAdd[i],
        );
      } else {
        iconsToAdd[i] = Container(
          width: 40,
          height: 40,
          decoration: BoxDecoration(
            // color: const Color(0xff90caf9),
            shape: BoxShape.circle,
          ),
          child: iconsToAdd[i],
        );
      }
    }

    return Container(
      height: 50,
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            width: 0,
            color: const Color(0xff5d99c6)
          )
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: iconsToAdd
      ),
    );
  }
}
